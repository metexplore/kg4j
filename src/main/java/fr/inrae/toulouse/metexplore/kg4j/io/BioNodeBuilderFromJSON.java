package fr.inrae.toulouse.metexplore.kg4j.io;

import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Utility class for constructing instances of {@link BioNode} subclasses from JSON objects.
 * Allows for dynamic construction of BioNode objects based on JSON attributes.
 *
 * @param <V> the type of BioNode to build
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioNodeBuilderFromJSON<V extends BioNode> {
    private final Supplier<V> instantiate;
    public List<String> nodeTypes;
    public String id;
    public String name;
    private final HashMap<BiConsumer<V, Object>, Function<JSONObject, Object>> buildStrategies = new HashMap<>();

    public BioNodeBuilderFromJSON(Supplier<V> instantiate) {
        this.instantiate = instantiate;
    }

    /**
     * Gets the default builder to create BioMetaboliteNode objects with basic attributes.
     * @return the default BioNodeBuilderFromJSON for BioMetaboliteNode nodes.
     */
    public static BioNodeBuilderFromJSON<BioMetaboliteNode> getDefaultBioMetaboliteNodeBuilder() {
        BioNodeBuilderFromJSON<BioMetaboliteNode> builder = new BioNodeBuilderFromJSON<>(BioMetaboliteNode::new);
        builder.addField(BioMetaboliteNode::setId, n -> n.getString("id"));
        builder.addField(BioMetaboliteNode::setName, n -> n.optString("name", "?"));
        return builder;
    }

    /**
     * Gets the default builder to create BioConceptNode objects with basic attributes.
     * @return the default BioNodeBuilderFromJSON for BioConceptNode nodes.
     */
    public static BioNodeBuilderFromJSON<BioConceptNode> getDefaultBioConceptNodeBuilder() {
        BioNodeBuilderFromJSON<BioConceptNode> builder = new BioNodeBuilderFromJSON<>(BioConceptNode::new);
        builder.addField(BioConceptNode::setId, n -> n.getString("id"));
        builder.addField(BioConceptNode::setName, n -> n.optString("name", "?"));
        return builder;
    }

    /**
     * Gets the default builder to create BioMetaboliteNode objects with basic attributes.
     * @return the default BioNodeBuilderFromJSON for BioConceptNode nodes.
     */
    public static BioNodeBuilderFromJSON<BioMetaboliteNode> getNeo4jBioMetaboliteNodeBuilder(){
        BioNodeBuilderFromJSON<BioMetaboliteNode> builder = new BioNodeBuilderFromJSON<>(BioMetaboliteNode::new);
        builder.addField(BioMetaboliteNode::setId, n -> n.getString("id"));
        builder.addField(BioMetaboliteNode::setName, n -> n.optString("name", "null"));
        builder.addField(BioMetaboliteNode::setDegree, n -> n.getDouble("degree"));
        return builder;
    }

    /**
     * Gets the default builder to create BioConceptNode objects with basic attributes.
     * @return the default BioNodeBuilderFromJSON for BioConceptNode nodes.
     */
    public static BioNodeBuilderFromJSON<BioConceptNode> getNeo4jBioConceptNodeBuilder(){
        BioNodeBuilderFromJSON<BioConceptNode> builder = new BioNodeBuilderFromJSON<>(BioConceptNode::new);
        builder.addField(BioConceptNode::setId, n -> n.getString("id"));
        builder.addField(BioConceptNode::setName, n -> n.optString("name", "null"));
        builder.addField(BioConceptNode::setDegree, n -> n.getDouble("degree"));
        builder.addField(BioConceptNode::addMeSHTreeNb, n -> n.optString("meSHTreeNb", "null"));
        builder.addField(BioConceptNode::addMeSHParentTreeNb, n -> n.optString("meSHParentTreeNb", "null"));
        return builder;
    }

    /**
     * Builds a new BioNode object from a JSON attributes map.
     * @param jsonEntry node as JSON entry.
     * @return BioNode object.
     */
    public V build(JSONObject jsonEntry) {
        V obj = instantiate.get();
        for (Map.Entry<BiConsumer<V, Object>, Function<JSONObject, Object>> step : buildStrategies.entrySet()) {
            step.getKey().accept(obj, step.getValue().apply(jsonEntry));
        }
        return obj;
    }

    /**
     * Adds a field to the builder, associating a setter function with a JSON attribute provider.
     * @param setter setter function for the field in the BioNode.
     * @param provider provider function for retrieving the attribute from an RDFNode.
     * @param <E> type of the field.
     */
    public <E extends Object> void addField(BiConsumer<V, E> setter, Function<JSONObject, E> provider) {
        buildStrategies.put((BiConsumer<V, Object>) setter, (Function<JSONObject, Object>) provider);
    }
}
