package fr.inrae.toulouse.metexplore.kg4j.data;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import fr.inrae.toulouse.metexplore.met4j_graph.core.BioGraph;

/**
 * Class defining a BioKnowledgeGraph object, subclass of {@link BioGraph}.
 * A BioKnowledgeGraph consists of {@link BioNode} nodes and {@link BioEdge} relations.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraph<V extends BioNode, E extends BioEdge<V, V>> extends BioGraph<V, E> {
    /**
     * Constructs an empty BioKnowledgeGraph.
     */
    public BioKnowledgeGraph(){
        super();
    }

    /**
     * Creates an edge from a model based on the provided biological entity and two vertices.
     * Currently, returns null as it is not implemented.
     *
     * @param v1 The biological entity from which the edge is to be created.
     * @param v2 The first vertex of the edge.
     * @param edge The edge model.
     * @return The created edge, currently returns null.
     */
    @Override
    public E createEdgeFromModel(V v1, V v2, E edge) {
        return (E) new BioEdge<V, V>(v1, v2, null);
    }

    /**
     * Creates a copy of the provided edge.
     * Currently, returns null as it is not implemented.
     *
     * @param edge The edge to be copied.
     * @return The copied edge, currently returns null.
     */
    @Override
    public E copyEdge(E edge) {
        return (E) new BioEdge<V, V>(edge.getV1(), edge.getV2(), null);
    }

    /**
     * Creates a vertex with the specified identifier.
     *
     * @param id The identifier for the vertex.
     * @return The created vertex.
     */
    @Override
    public V createVertex(String id) {
        V vertex = this.createVertex();
        vertex.setId(id);
        return vertex;
    }

    /**
     * Creates a new edge.
     * @return The created edge.
     */
    @Override
    public E createEdge() {
        return super.createEdge();
    }

    /**
     * Creates an edge between two specified vertices.
     * Currently, returns null as it is not implemented.
     *
     * @param v1 The first vertex.
     * @param v2 The second vertex.
     * @return The created edge, currently returns null.
     */
    @Override
    public E createEdge(V v1, V v2){
        return (E) new BioEdge<V, V>(v1, v2, null);
    }

    /**
     * Transforms the graph as a list of triplets : subject -- predicate -> object.
     * @return String graph as a String.
     */
    public String graphToString() {
        String graph = "";
        // Iterates over all the edges in the graph
        for (E e : this.edgeSet()) {
            BioNode source = this.getEdgeSource(e);
            BioNode target = this.getEdgeTarget(e);
            graph += source.getName() + " -- " + e.getLabel() + " -> " + target.getName() + "\n";
            // add more attributes to display
        }
        return graph;
    }
}
