package fr.inrae.toulouse.metexplore.kg4j.script;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.riot.RDFDataMgr;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioKnowledgeGraphBuilder;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.RDF.FORUMBinding.FORUMUtils;
import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioKnowledgeGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import fr.inrae.toulouse.metexplore.kg4j.edit.BioKnowledgeGraphEditor;
import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphExporter;
import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphJSONImporter;
import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphNeo4jJSONImporter;
import fr.inrae.toulouse.metexplore.kg4j.io.BioNode2Attribute;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static fr.inrae.toulouse.metexplore.kg4j.script.FORUMAPI.addMeSHThesaurus;
import static fr.inrae.toulouse.metexplore.kg4j.script.FORUMAPI.filterCompounds;

/**
 * Entry point of the program.
 * Executes various operations related to building, editing, and analyzing BioKnowledgeGraph graphs.
 * This method performs the following tasks:
 * <ol>
 *     <li>Load RDF data from a file into a Jena model</li>
 *     <li>Execute a SPARQL query to retrieve specific information from the RDF data</li>
 *     <li>Construct a graph based on the query results using custom builders</li>
 *     <li>Export the constructed graph to a JSON file</li>
 *     <li>Import a graph from a JSON file and perform editing operations on it</li>
 *     <li>Perform various operations and analyses on the graph using JGraphT algorithms</li>
 * </ol>
 * This method relies on several utility classes and methods to accomplish its tasks, including RDFUtils, BuilderUtils, GraphBuilder,
 * GraphExporter, GraphJSONImporter, GraphEditor, and various JGraphT algorithms.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 * @throws IOException if an I/O error occurs while reading the RDF file or JSON file
 */
public class Example {
    public static void main(String[] args) throws IOException, InterruptedException {

        //------------------------------------------------------//
        //                  LOAD MODEL (JENA)                   //
        //------------------------------------------------------//

        // Load model from RDF file
        Model model = RDFDataMgr.loadModel("test_graph.rdf");

        // SPARQL query from endpoint
        // Model model2 = loadModelSPARQLQuery("SPARQL_query.txt");

        System.out.println("Model loaded");

        //------------------------------------------------------//
        //               ENDOMETRIOSIS KG FILTERS               //
        //------------------------------------------------------//

        // Filter nodes using a list of IDs in a file
        // Example : keep only PFAS compounds
        model = filterCompounds(model, "/home/mmathe/Documents/EndometriosisKG/fluorocarbons/PFAS_compounds_ids.txt");

        // Add links in the graph using a list of pairs of IDs in a file
        // Example : add MeSH concepts thesaurus information (hierachical links)
        model = addMeSHThesaurus(model);

        //------------------------------------------------------//
        //                    CUSTOM BUILDERS                   //
        //------------------------------------------------------//

        BioNodeBuilder<BioConceptNode> meshConceptBuilder = FORUMUtils.DefaultBioConceptNodeBuilder(model);
        BioNodeBuilder<BioMetaboliteNode> compoundBuilder = FORUMUtils.DefaultBioMetaboliteNodeBuilder(model);
        BioKnowledgeGraphBuilder KGBuilder = new BioKnowledgeGraphBuilder(model);

        //------------------------------------------------------//
        //                   SELECTORS (JENA)                   //
        //------------------------------------------------------//

        // MeSH concepts selector
        SimpleSelector meshSelector = FORUMUtils.DefaultMeSHSelector(model);
        SimpleSelector compoundSelector = FORUMUtils.DefaultCompoundSelector(model);
        SimpleSelector relationSelector = FORUMUtils.DefaultRelationSelector(model);
        SimpleSelector subClassSelector = FORUMUtils.MeSHSubClassSelector(model);


        //------------------------------------------------------//
        //                 BUILD GRAPH (JGraphT)                //
        //------------------------------------------------------//

        // Add nodes
        KGBuilder.addNodes(meshSelector, meshConceptBuilder);
        KGBuilder.addNodes(compoundSelector, compoundBuilder);
        // Add edges
        KGBuilder.addEdges(relationSelector, "related");
        KGBuilder.addEdges(subClassSelector, "subClassOf");
        // Print graph in console
        // System.out.println(KGBuilder.graphToString());

        // Remove endometriosis node
        BioKnowledgeGraphEditor editor = new BioKnowledgeGraphEditor(KGBuilder.getKG());
        BioNode endoNode = KGBuilder.KG.vertexSet().stream().filter(bioNode -> bioNode.getName().equals("Endometriosis")).findAny().orElse(null);
        editor.removeVertexAndEdges(endoNode.getId());

        // Export graph
        BioKnowledgeGraphExporter KGExporter = new BioKnowledgeGraphExporter();
        KGExporter.KG = KGBuilder.getKG();
        KGExporter.attResolver.put("metabolite", BioNode2Attribute.getDefaultBioMetaboliteNode2Attributes());
        KGExporter.attResolver.put("concept", BioNode2Attribute.getDefaultBioConceptNode2Attributes());
        KGExporter.graphToJSON("/target/KG_endometriosis_test.json");


        //------------------------------------------------------//
        //                IMPORT GRAPH FROM JSON                //
        //------------------------------------------------------//

        // Set nodes importer
        BioKnowledgeGraphJSONImporter JSONimporter = BioKnowledgeGraphJSONImporter.getDefaultImporter();
        // Import JSON graph from neo4j
        BioKnowledgeGraphNeo4jJSONImporter importer = BioKnowledgeGraphNeo4jJSONImporter.getDefaultImporter();
        // Load graph from JSON file
        Path path = Paths.get("/home/mmathe/Documents/JAVA/PokeGraph/src/main/resources/test_export.json");
        String json = new Scanner(path.toFile()).useDelimiter("\\Z").next();
        JSONimporter.build(json);
        // Print graph in console
        // System.out.println(JSONimporter.getKG());


        //------------------------------------------------------//
        //                     EDIT GRAPH                       //
        //------------------------------------------------------//
        BioKnowledgeGraphEditor KGeditor = new BioKnowledgeGraphEditor(JSONimporter.getKG());
        // Add node
        BioMetaboliteNode newMetabolite = new BioMetaboliteNode();
        newMetabolite.setName("new compound");
        newMetabolite.setId("compound1");
        KGeditor.addNewVertex(newMetabolite);
        // Add edges
        BioNode target = KGeditor.KG.vertexSet().stream().filter(bioNode -> bioNode.getName().equals("Cicatrix")).findAny().orElse(null);
        KGeditor.addNewBioEdge(newMetabolite.getId(), target.getId(), "related");
        // Delete node
        BioNode node2remove = KGeditor.KG.vertexSet().stream().filter(bioNode -> bioNode.getName().equals("new compound")).findAny().orElse(null);
        KGeditor.removeVertexAndEdges(node2remove.getId());
        // Print edited graph
        System.out.println(KGeditor.graphToString());


        //------------------------------------------------------//
        //                  EDIT GRAPH (met4j)                  //
        //------------------------------------------------------//
        // Create empty BioKnowledgeGraph
        BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> graph = new BioKnowledgeGraph<>();
        // Create vertices
        BioNode node1 = graph.createVertex("endometriosis");
        BioNode node2 = graph.createVertex("Il-17");
        // Create an edge between node1 and node2
        BioEdge<BioNode, BioNode> edge1 = graph.createEdge(node1, node2);
        // Add vertixes and edges to the graph
        graph.addVertex(node1);
        graph.addVertex(node2);
        graph.addEdge(edge1);
        graph.addEdge(edge1);
        // Print graph
        System.out.println(graph.graphToString());

        //------------------------------------------------------//
        //             GRAPH OPERATIONS (JGraphT)               //
        //------------------------------------------------------//
        BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> KG = KGeditor.KG;
        // Size and connectivity
        int vertexCount = KG.vertexSet().size();
        int edgeCount = KG.edgeSet().size();
//        double diameter = GraphMetrics.getDiameter(KG);
//        double radius = GraphMetrics.getRadius(KG);
//        ConnectivityInspector<BioNode, BioEdge<BioNode, BioNode>> connectivityInspector = new ConnectivityInspector<>(KG);
//        boolean isConnected = connectivityInspector.isConnected();
        // Shortest path related metrics
        DijkstraShortestPath<BioNode, BioEdge<BioNode, BioNode>> shortestPath = new DijkstraShortestPath<>(KG);
        // Output
        System.out.println("Vertex count: " + vertexCount);
        System.out.println("Edge count: " + edgeCount);
//        System.out.println("Is connected: " + isConnected);
//        System.out.println("Diameter: " + diameter);
//        System.out.println("Radius: " + radius);
    }
}
