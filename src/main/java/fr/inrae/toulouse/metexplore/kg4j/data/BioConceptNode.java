package fr.inrae.toulouse.metexplore.kg4j.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class defining a BioConceptNode object, subclass of {@link BioNode}.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioConceptNode extends BioNode{
    public String id;
    public String name;
    public Double degree;
    public List<String> meSHtreeNb = new ArrayList<>();
    public List<String> parentMeSHTreeNb = new ArrayList<>();
    public List<String> nodeTypes = new ArrayList<>();

    /**
     * Class constructor specifying the new BioConceptNode object's ID.
     * @param id node ID
     */
    public BioConceptNode(String id, String name) {
        super(id, name);
        this.id = id;
        this.name = name;
        this.nodeTypes.add("concept");
    }

    /**
     * Class constructor with a randomly generated ID.
     * Uses {@link UUID#randomUUID()} to generate a unique identifier.
     */
    public BioConceptNode() {
        super(null, null);
        this.nodeTypes.add("concept");
    }

    /**
     * Get label.
     * @return BioConceptNode object's name (String).
     */
    public String getName(){
        return this.name;
    }

    /**
     * Get ID.
     * @return BioConceptNode object's ID (String).
     */
    public String getId(){
        return this.id;
    }

    public String getMeSHCategory() {
        if(this.meSHtreeNb.get(0) != null){
            return String.valueOf(this.meSHtreeNb.get(0).charAt(0));
        } else {
            return "null";
        }
    }

    /**
     * Get degree.
     * @return BioConceptNode object's degree (Double).
     */
    public Double getDegree() {
        return degree;
    }

    /**
     * Get type.
     * @return BioConceptNode object's node type (String).
     */
    public List<String> getNodeTypes(){
        return this.nodeTypes;
    }

    /**
     * Get MeSH tree numbers.
     * @return MeSH tree numbers.
     */
    public List<String> getMeSHtreeNb(){
        return this.meSHtreeNb;
    }

    /**
     * Get MeSH parent tree numbers.
     * @return MeSH parent tree numbers.
     */
    public List<String> getParentMeSHTreeNb(){
        return this.parentMeSHTreeNb;
    }

    /**
     * Sets the name of this node.
     * @param name The name to set.
     */
    @Override
    public void setName(String name){
        this.name = name;
    }

    /**
     * Sets the degree of this node.
     * @param degree The degree to set.
     */
    public void setDegree(Double degree){ this.degree = degree;}

    /**
     * Adds a MeSH tree number to this node.
     * @param treeNb The MeSH tree number to add.
     */
    public void addMeSHTreeNb(String treeNb){
        this.meSHtreeNb.add(treeNb);
    }

    /**
     * Adds a parent MeSH tree number to this node.
     * @param parentTreeNb The parent MeSH tree number to add.
     */
    public void addMeSHParentTreeNb(String parentTreeNb){
        this.parentMeSHTreeNb.add(parentTreeNb);
    }

    /**
     * Adds a type to this node.
     * @param type The type to add.
     */
    public void addType(String type){
        this.nodeTypes.add(type);
    }

    /**
     * Sets the ID of this node.
     * @param id The ID to set.
     * @return This BioConceptNode object.
     */
    public BioConceptNode setId(String id){
        this.id = id;
        return this;
    }
}
