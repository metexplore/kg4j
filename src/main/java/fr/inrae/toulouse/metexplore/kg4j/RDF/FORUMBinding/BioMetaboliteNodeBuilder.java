package fr.inrae.toulouse.metexplore.kg4j.RDF.FORUMBinding;

import org.apache.jena.rdf.model.Model;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.RDF.RDFUtils;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;

/**
 * Class implementing methods to build a BioMetaboliteNode object
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioMetaboliteNodeBuilder extends BioNodeBuilder<BioMetaboliteNode> {

    /**
     * Class constructor.
     * Constructs a BioMetaboliteNodeBuilder with the specified RDF model.
     * Initializes the BioMetaboliteNode instances and defines the fields to extract attributes from the RDF model.
     * @param model the RDF model containing information about the nodes
     */
    public BioMetaboliteNodeBuilder(Model model) {
        super(model, BioMetaboliteNode::new);
        // Define fields to extract attributes from the RDF model
        this.addField(BioMetaboliteNode::setName, node -> RDFUtils.getAttribute(node.toString(), "http://www.w3.org/2000/01/rdf-schema#label", model, (x) -> x.toString()));
    }
}