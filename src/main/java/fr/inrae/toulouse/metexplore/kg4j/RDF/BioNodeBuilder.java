package fr.inrae.toulouse.metexplore.kg4j.RDF;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Abstract class implementing methods to build a BioNode object
 *
 * @param <V> type of BioNode to build
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioNodeBuilder<V extends BioNode> {
    private final Supplier<V> instantiate;
    public String nodeType;
    public String id;
    public Model model;
    /**
     * Map for building strategies, associating setter functions with RDFNode attribute providers
     */
    private final HashMap<BiConsumer<V, Object>, Function<RDFNode, Object>> buildStrategies = new HashMap<>();

    /**
     * Class constructor.
     * @param model the RDF model containing information about the nodes.
     */
    public BioNodeBuilder(Model model, Supplier<V> instantiate) {
        this.model = model;
        this.instantiate = instantiate;
    }

    /**
     * Builds a new BioNode object from a given resource in a JENA model.
     * @param node RDF resource.
     * @return BioNode object.
     */
    public V build(RDFNode node) {
        V obj = instantiate.get();
        for (Map.Entry<BiConsumer<V, Object>, Function<RDFNode, Object>> step : buildStrategies.entrySet()) {
            step.getKey().accept(obj, step.getValue().apply(node));
        }
        return obj;
    }

    /**
     * Adds a field to the builder, associating a setter function with an RDFNode attribute provider.
     * @param setter setter function for the field in the BioNode.
     * @param provider provider function for retrieving the attribute from an RDFNode.
     * @param <E> type of the field.
     */
    public <E extends Object> void addField(BiConsumer<V, E> setter, Function<RDFNode, E> provider) {
        buildStrategies.put((BiConsumer<V, Object>) setter, (Function<RDFNode, Object>) provider);
    }
}
