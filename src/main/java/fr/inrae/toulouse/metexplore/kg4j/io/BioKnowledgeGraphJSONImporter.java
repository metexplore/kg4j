package fr.inrae.toulouse.metexplore.kg4j.io;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import fr.inrae.toulouse.metexplore.kg4j.data.BioKnowledgeGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class implementing methods to import a JGraphT graph from a JSON file.
 *
 * @author Clément Frainay
 * @author Meije Mathé
 */
public class BioKnowledgeGraphJSONImporter {
    public BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> KG;
    // public String typeKey = "type";
    public List<String> multiTypeKey = Arrays.asList("type", "type2");
    public String edgeLabelKey = "label";
    public String sourceKey = "source";
    public String targetKey = "target";
    public String nodeKey = "nodes";
    public String edgeKey = "edges";
    // Map associating a node builder to a node type
    public Map<String, BioNodeBuilderFromJSON> nodeResolver = new HashMap<>();
    BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> newGraph;
    public List<String> getTypeKeys() {
        return multiTypeKey;
    }

    public void setMultiTypeKey(List<String> multiTypeKey) {
        this.multiTypeKey = multiTypeKey;
    }

    public String getEdgeLabelKey() {
        return edgeLabelKey;
    }

    public void setEdgeLabelKey(String edgeLabelKey) {
        this.edgeLabelKey = edgeLabelKey;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getTargetKey() {
        return targetKey;
    }

    public void setTargetKey(String targetKey) {
        this.targetKey = targetKey;
    }

    public String getNodeKey() {
        return nodeKey;
    }

    public void setNodeKey(String nodeKey) {
        this.nodeKey = nodeKey;
    }

    public String getEdgeKey() {
        return edgeKey;
    }

    public void setEdgeKey(String edgeKey) {
        this.edgeKey = edgeKey;
    }

    /**
     * Class constructor.
     * Initializes the new graph.
     */
    public BioKnowledgeGraphJSONImporter() {
        newGraph = new BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>>();
    }

    /**
     * Gets default importer.
     * Associates a node builder for each node type.
     * @return a graph importer with a default nodeResolver map.
     */
    public static BioKnowledgeGraphJSONImporter getDefaultImporter() {
        BioKnowledgeGraphJSONImporter imp = new BioKnowledgeGraphJSONImporter();
        imp.nodeResolver.put("metabolite", BioNodeBuilderFromJSON.getDefaultBioMetaboliteNodeBuilder());
        imp.nodeResolver.put("concept", BioNodeBuilderFromJSON.getDefaultBioConceptNodeBuilder());
        return imp;
    }

    /**
     * Builds the new graph from the contents of a JSON file.
     * @param json contents of a JSON file in a a String object.
     */
    public void build(String json) {
        JSONObject root = new JSONObject(json);
        // Use the nodeKey to retrieve a JSONArray containing the nodes
        JSONArray nodes = root.getJSONArray(this.nodeKey);
        // Use the edgeKey to retrieve a JSONArray containing the edges
        JSONArray edges = root.getJSONArray(this.edgeKey);
        // Build the new graph using these JSONArrays
        build(nodes, edges);
    }

    /**
     * Builds the new graph from JSONArrays of nodes and edges.
     * @param nodes JSONArray of nodes
     * @param edges JSONArray of edges
     */
    public void build(JSONArray nodes, JSONArray edges) {
        // Nodes
        // Iterate over each node in the JSONArray
        for (int i = 0; i < nodes.length(); i++) {
            JSONObject entry = new JSONObject(nodes.get(i).toString());
            // Use the node type to select the node builder
            // The node type is the first type in the list of possible types
            BioNodeBuilderFromJSON builder = nodeResolver.get(entry.getString(multiTypeKey.get(0)));
            BioNode node = builder.build(entry);
            // Iterate over the other types in the list
            for(String typeKey : multiTypeKey.subList(1, multiTypeKey.size())){
                // Check if the other types exist in the JSON entry
                if(entry.has(typeKey)){
                    // If the new type isn't in the new node's type list, add it
                    if(!node.getNodeTypes().contains(entry.getString(typeKey))){
                        node.addType(entry.getString(typeKey));
                    }
                }
            }
            newGraph.addVertex(node);
            // Build a new node from the JSON entry and add it to the graph
        }
        // Edges
        // Iterate over each edge in the JSONArray
        for (int i = 0; i < edges.length(); i++) {
            JSONObject entry = new JSONObject(edges.get(i).toString());
            // Extract edge atributes
            String label = entry.getString(edgeLabelKey);
            String source = entry.getString(sourceKey);
            String target = entry.getString(targetKey);
            // Find the source and target nodes in the graph using IDs
            // Return null if the node doesn't exist in the graph
            BioNode v1 = newGraph.vertexSet().stream().filter(bioNode -> bioNode.getId().equals(source)).findAny().orElse(null);
            BioNode v2 = newGraph.vertexSet().stream().filter(bioNode -> bioNode.getId().equals(target)).findAny().orElse(null);
            if (v1 == null || v2 == null) {
                // Handle the case where one of the nodes doesn't exist : throw an IllegalArgumentException:
                throw new IllegalArgumentException("One of the nodes does not exist in the graph.");
            } else {
                // Build a new edge and add it to the graph
                BioEdge<BioNode, BioNode> newEdge = new BioEdge<>(v1, v2,null);
                newEdge.setLabel(label);
                newGraph.addEdge(newEdge);
            }
        }
    }

    /**
     * Get graph.
     * @return the new imported graph.
     */
    public BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> getKG() {
        return newGraph;
    }
}
