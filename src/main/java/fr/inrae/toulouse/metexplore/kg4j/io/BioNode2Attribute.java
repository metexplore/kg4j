package fr.inrae.toulouse.metexplore.kg4j.io;

import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Utility class for converting attributes of a generic BioNode to a set of key-value pairs
 * Uses specifying attribute getters for string, integer, and double attributes.
 *
 * @param <V> type of BioNode for which the attributes are converted
 * @author Clément Frainay
 * @author Meije Mathé
 **/
public class BioNode2Attribute <V extends BioNode> {
    private final Map<String, Function<V, String>> stringAttributeGetter = new HashMap<>();
    private final Map<String, Function<V, Integer>> intAttributeGetter = new HashMap<>();
    private final Map<String, Function<V, Double>> doubleAttributeGetter = new HashMap<>();
    private final Map<String, Function<V, List<String>>> listStringAttributeGetter = new HashMap<>();

    /**
     * Gets the default BioNode2Attribute converter with basic attributes.
     * @return default BioNode2Attribute converter.
     */
    public static BioNode2Attribute<? extends BioNode> getDefaultNode2Attributes() {
        BioNode2Attribute<? extends BioNode> converter = new BioNode2Attribute<>();
        converter.addStringAttribute("id", BioNode::getId);
        converter.addStringAttribute("name", BioNode::getName);
        converter.addListStringAttribute("type", BioNode::getNodeTypes);
        return converter;
    }

    /**
     * Creates a converter to get the BioMetaboliteNode's attributes : specifies a getter for each attribute.
     * @return BioNode2Attribute<BioMetaboliteNode> converter.
     */
    public static BioNode2Attribute<BioMetaboliteNode> getDefaultBioMetaboliteNode2Attributes() {
        BioNode2Attribute<BioMetaboliteNode> converter = new BioNode2Attribute<>();
        converter.addStringAttribute("id", BioMetaboliteNode::getId);
        converter.addStringAttribute("name", BioMetaboliteNode::getName);
        converter.addListStringAttribute("type", BioMetaboliteNode::getNodeTypes);
        return converter;
    }

    /**
     * Creates a converter to get the BioConceptNode's attributes : specifies a getter for each attribute.
     * @return BioNode2Attribute<BioConceptNode> converter.
     */
    public static BioNode2Attribute<BioConceptNode> getDefaultBioConceptNode2Attributes() {
        BioNode2Attribute<BioConceptNode> converter = new BioNode2Attribute<>();
        converter.addStringAttribute("id", BioConceptNode::getId);
        converter.addStringAttribute("name", BioConceptNode::getName);
        converter.addListStringAttribute("type", BioConceptNode::getNodeTypes);
        converter.addListStringAttribute("meSHTreeNb", BioConceptNode::getMeSHtreeNb);
        converter.addListStringAttribute("meSHParentTreeNb", BioConceptNode::getParentMeSHTreeNb);
        converter.addStringAttribute("category", BioConceptNode::getMeSHCategory);
        return converter;
    }

    /**
     * Creates a converter to get the BioMetaboliteNode's attributes : specifies a getter for each attribute.
     * @return BioNode2Attribute<BioConceptNode> converter.
     */
    public static BioNode2Attribute<BioMetaboliteNode> getNeo4jBioMetaboliteNode2Attribute(){
        BioNode2Attribute<BioMetaboliteNode> converter = new BioNode2Attribute<>();
        converter.addStringAttribute("id", BioMetaboliteNode::getId);
        converter.addStringAttribute("name", BioMetaboliteNode::getName);
        converter.addListStringAttribute("type", BioMetaboliteNode::getNodeTypes);
        converter.addDoubleAttribute("degree", BioMetaboliteNode::getDegree);
        return converter;
    }

    /**
     * Creates a converter to get the BioConceptNode's attributes : specifies a getter for each attribute.
     * @return BioNode2Attribute<BioConceptNode> converter.
     */
    public static BioNode2Attribute<BioConceptNode> getNeo4jBioConceptNode2Attribute(){
        BioNode2Attribute<BioConceptNode> converter = new BioNode2Attribute<>();
        converter.addStringAttribute("id", BioConceptNode::getId);
        converter.addStringAttribute("name", BioConceptNode::getName);
        converter.addListStringAttribute("type", BioConceptNode::getNodeTypes);
        converter.addListStringAttribute("meSHTreeNb", BioConceptNode::getMeSHtreeNb);
        converter.addListStringAttribute("meSHParentTreeNb", BioConceptNode::getParentMeSHTreeNb);
        converter.addStringAttribute("category", BioConceptNode::getMeSHCategory);
        converter.addDoubleAttribute("degree", BioConceptNode::getDegree);
        return converter;
    }

    /**
     * Adds a String typed attribute's name and its corresponding getter function.
     * @param attName name of the attribute.
     * @param getAttribute getter = function to get the attribute's value for a given BioNode.
     */
    public void addStringAttribute(String attName, Function<V, String> getAttribute) {
        stringAttributeGetter.put(attName, getAttribute);
    }

    /**
     * Adds a List<String> typed attribute's name and its corresponding getter function.
     * @param attName name of the attribute.
     * @param getAttribute getter = function to get the attribute's value for a given BioNode.
     */
    public void addListStringAttribute(String attName, Function<V, List<String>> getAttribute){
        listStringAttributeGetter.put(attName, getAttribute);
    }

    /**
     * Adds an Integer typed attribute's name and its corresponding getter function.
     * @param attName name of the attribute.
     * @param getAttribute getter = function to get the attribute's value for a given BioNode.
     */
    public void addIntAttribute(String attName, Function<V, Integer> getAttribute) {
        intAttributeGetter.put(attName, getAttribute);
    }

    /**
     * Adds a Double typed attribute's name and its corresponding getter function.
     * @param attName name of the attribute.
     * @param getAttribute getter = function to get the attribute's value for a given BioNode.
     */
    public void addDoubleAttribute(String attName, Function<V, Double> getAttribute) {
        doubleAttributeGetter.put(attName, getAttribute);
    }

    /**
     * Retrieves String typed attributes for a given BioNode using the adapted getter.
     * @param n BioNode for which attributes are to be retrieved.
     * @return a map containing String attributes' names with their corresponding values.
     */
    public Map<String, String> getStringAttributes(V n) {
        Map<String, String> att = new HashMap<>();
        for (Map.Entry<String, Function<V, String>> e : stringAttributeGetter.entrySet()) {
            att.put(e.getKey(), e.getValue().apply(n));
        }
        return att;
    }

    /**
     * Retrieves List<String> typed attributes for a given BioNode using the adapted getter.
     * @param n BioNode for which attributes are to be retrieved.
     * @return a map containing String attributes' names with their corresponding values.
     */
    public Map<String, String> getListStringAttributes(V n){
        Map<String, String> att = new HashMap<>();
        for (Map.Entry<String, Function<V, List<String>>> e : listStringAttributeGetter.entrySet()){
            Integer nbAtt = 0;
            for (String type : e.getValue().apply(n)){
                nbAtt ++;
                if(nbAtt > 1){
                    att.put(e.getKey() + nbAtt.toString(), type);
                }else{
                    att.put(e.getKey(), type);
                }
            }
        }
        return att;
    }

    /**
     * Retrieves Integer typed attributes for a given BioNode using the adapted getter.
     * @param n BioNode for which attributes are to be retrieved.
     * @return a map containing Integer attributes' names with their corresponding values.
     */
    public Map<String, Integer> getIntAttributes(V n) {
        Map<String, Integer> att = new HashMap<>();
        for (Map.Entry<String, Function<V, Integer>> e : intAttributeGetter.entrySet()) {
            att.put(e.getKey(), e.getValue().apply(n));
        }
        return att;
    }

    /**
     * Retrieves Double typed attributes for a given BioNode using the adapted getter.
     * @param n BioNode for which attributes are to be retrieved.
     * @return a map containing Double attributes' names with their corresponding values.
     */
    public Map<String, Double> getDoubleAttributes(V n) {
        Map<String, Double> att = new HashMap<>();
        for (Map.Entry<String, Function<V, Double>> e : doubleAttributeGetter.entrySet()) {
            att.put(e.getKey(), e.getValue().apply(n));
        }
        return att;
    }

    /**
     * Converts the attributes of a V object using the provided BioNode2Attribute<V> converter.
     * @param n node for which the attributes are converted.
     * @return the map created by the converter.
     */
    public Map<String, Attribute> getAttributes(V n) {
        Map<String, Attribute> resultAttributes = new LinkedHashMap<>();
        // Convert string attributes
        for (Map.Entry<String, String> attribute : this.getStringAttributes(n).entrySet()) {
            resultAttributes.put(attribute.getKey(), DefaultAttribute.createAttribute(attribute.getValue()));
        }
        for (Map.Entry<String, String> attribute : this.getListStringAttributes(n).entrySet()){
            resultAttributes.put(attribute.getKey(), DefaultAttribute.createAttribute(attribute.getValue()));
        }
        // Convert integer attributes
        for (Map.Entry<String, Integer> attribute : this.getIntAttributes(n).entrySet()) {
            resultAttributes.put(attribute.getKey(), DefaultAttribute.createAttribute(attribute.getValue()));
        }
        // Convert double attribute
        for (Map.Entry<String, Double> attribute : this.getDoubleAttributes(n).entrySet()) {
            resultAttributes.put(attribute.getKey(), DefaultAttribute.createAttribute(attribute.getValue()));
        }
        return resultAttributes;

    }
}
