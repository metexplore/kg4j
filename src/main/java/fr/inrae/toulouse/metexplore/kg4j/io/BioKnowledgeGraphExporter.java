package fr.inrae.toulouse.metexplore.kg4j.io;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import fr.inrae.toulouse.metexplore.kg4j.data.BioKnowledgeGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.json.JSONExporter;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Class implementing methods to export a JGraphT graph in a JSON file.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphExporter {
    public BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> KG;
    // Map defining an attribute getter depending on the node type
    public Map<String, BioNode2Attribute> attResolver = new HashMap<>();

    /**
     * Creates a default GraphExporter object.
     * Associates an attribute getter for each node type.
     * @return a graph exporter with a predefined attResolver map.
     */
    public static BioKnowledgeGraphExporter getDefaultExporter() {
        BioKnowledgeGraphExporter ex = new BioKnowledgeGraphExporter();
        ex.attResolver.put("metabolite", BioNode2Attribute.getDefaultBioMetaboliteNode2Attributes());
        ex.attResolver.put("concept", BioNode2Attribute.getDefaultBioConceptNode2Attributes());
        return ex;
    }

    /**
     * Exports the JGraphT graph in JSON format.
     * @param fileName name of the JSON file to export the graph to.
     */
    public void graphToJSON(String fileName) {
        // Vertex attribute provider : define node attributes to put in the JSON file
        // Iterate over each vertex (BioNode v) in the graph and retrieve the attributes
        // Return the vertex's attributes in a Map<String, Attribute> (Map<attribute name, attribute value>)
        Function<BioNode, Map<String, Attribute>> vertexAttributeProvider = (v) -> {
            // Select an attribute provider according to the vertex's node type
            BioNode2Attribute converter = attResolver.get(v.getNodeTypes().get(0));
            // Use a default attribute converter if none is defined for the node type
            if (converter == null) {
                converter = BioNode2Attribute.getDefaultNode2Attributes();
            }
            return converter.getAttributes(v);
        };
        // Edge attribute provider : define edge attributes to put in the JSON file
        // Iterate over each edge (BioEdge e) in the graph and retrieve the attributes
        // Return the edge's attributes in a Map<String, Attribute> (Map<attribute name, attribute value>)
        Function<BioEdge<BioNode, BioNode>, Map<String, Attribute>> edgeAttributeProvider = e -> {
            Map<String, Attribute> map = new LinkedHashMap<>();
            // Add the edge's label to the attribute map
            map.put("label", DefaultAttribute.createAttribute(e.getLabel()));
            return map;
        };
        // JSON graph exporter
        // BioNode::getId = vertexIdProvider to provide an ID to each vertex (BioNode ID)
        JSONExporter<BioNode, BioEdge<BioNode, BioNode>> exporter = new JSONExporter<>(BioNode::getId);
        // Parametrize the JSON graph exporter with the vertex and edges attribute providers
        exporter.setVertexAttributeProvider(vertexAttributeProvider);
        exporter.setEdgeAttributeProvider(edgeAttributeProvider);
        // Export graph to file
        exporter.exportGraph(this.KG, new File("./" + fileName));
    }

}
