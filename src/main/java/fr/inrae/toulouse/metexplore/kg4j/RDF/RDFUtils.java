package fr.inrae.toulouse.metexplore.kg4j.RDF;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Utility class to retrieve attributes of BioNode objects from a JENA RDF model.
 * Provides a method to retrieve any attribute of a BioNode by querying a JENA model.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class RDFUtils {
    /**
     * Retrieves any BioNode's attribute by querying a JENA model.
     * @param subject subject node's label.
     * @param property attribute to retrieve.
     * @param model JENA model.
     * @param operation the function to apply on the retrieved RDFNode to extract the attribute value.
     * @param <AttrType> the type of the attribute to retrieve.
     * @return the attribute value of type AttrType.
     */
    public static <AttrType> AttrType getAttribute(String subject, String property, Model model, Function<RDFNode, AttrType> operation) {
        // Filter JENA model
        Predicate<Statement> predicate = stmt ->
                stmt.getSubject().equals(model.getResource(subject)) &&
                        stmt.getPredicate().equals(model.getProperty(property));
        // Get attribute : query result
        StmtIterator selection = model.listStatements();
        AttrType attribute = null;
        while (selection.hasNext()) {
            Statement stmt = selection.nextStatement();
            if (predicate.test(stmt)) {
                attribute = operation.apply(stmt.getObject());
            }
        }
        return attribute;
    }
}
