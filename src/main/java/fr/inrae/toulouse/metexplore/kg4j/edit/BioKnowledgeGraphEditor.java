package fr.inrae.toulouse.metexplore.kg4j.edit;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import fr.inrae.toulouse.metexplore.kg4j.data.BioKnowledgeGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;

/**
 * Class wrapping methods to edit a JGraphT graph : add / remove nodes and edges, convert graph to String.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphEditor {
    public BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> KG;

    /**
     * Class constructor.
     * Initializes the BioKnowledgeGraphEditor with an empty {@link BioKnowledgeGraph}.
     */
    public BioKnowledgeGraphEditor() {
        this.KG = null;
    }

    /**
     * Class constructor specifying the graph to edit.
     * @param graph the graph to edit.
     */
    public BioKnowledgeGraphEditor(BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> graph) {
        this.KG = graph;
    }

    /**
     * Adds a new node (BioNode) in the JGraphT graph.
     * @param vertex the new {@link BioNode} object to add.
     */
    public void addNewVertex(BioNode vertex) {
        this.KG.addVertex(vertex);
    }

    /**
     * Adds a new edge (BioEdge<BioNode, Bionode> in the JGraphT graph.
     * @param source the ID of the new edge's source node.
     * @param target the ID of the new edge's target node.
     * @param label  label of the edge.
     */
    public void addNewBioEdge(String source, String target, String label) {
        // Find the source and target nodes in the graph : filter on the nodes' IDs
        // If the node doesn't exist (wrong ID), return null
        BioNode s = this.KG.vertexSet().stream().filter(bioNode -> bioNode.getId().equals(source)).findAny().orElse(null);
        BioNode t = this.KG.vertexSet().stream().filter(bioNode -> bioNode.getId().equals(target)).findAny().orElse(null);
        // Check if either source or target node is null
        if (s == null || t == null) {
            // Handle the case where one of the nodes doesn't exist : throw an IllegalArgumentException:
            throw new IllegalArgumentException("One of the nodes does not exist in the graph.");
        } else {
            // Create a new BioEdge object and add it to the graph
            BioEdge<BioNode, BioNode> newEdge = new BioEdge<>(s, t,null);
            newEdge.setLabel(label);
            this.KG.addEdge(newEdge);
        }
    }

    /**
     * Removes a node (BioNode) and all the connecting edges in the JGraphT graph.
     * @param vertex the ID of the node to remove.
     */
    public void removeVertexAndEdges(String vertex) {
        // Find the node to remove in the graph : filter on the node's ID
        // If the node doesn't exist (wrong ID), return null
        BioNode v = KG.vertexSet().stream().filter(bioNode -> bioNode.getId().equals(vertex)).findAny().orElse(null);
        if (v == null) {
            // Handle the case where one of the nodes doesn't exist : throw an IllegalArgumentException:
            throw new IllegalArgumentException("The node does not exist in the graph.");
        } else {
            // Remove vertex and all connecting edges
            this.KG.removeVertex(v);
        }
    }

    /**
     * Transforms the graph as a list of triplets : subject -- predicate -> object.
     * @return String graph as a String.
     */
    public String graphToString() {
        String graph = "";
        // Iterates over all the edges in the graph
        for (BioEdge e : this.KG.edgeSet()) {
            BioNode source = this.KG.getEdgeSource(e);
            BioNode target = this.KG.getEdgeTarget(e);
            graph += source.getName() + " -- " + e.getLabel() + " -> " + target.getName() + "\n";
            // add more attributes to display
        }
        return graph;
    }
}
