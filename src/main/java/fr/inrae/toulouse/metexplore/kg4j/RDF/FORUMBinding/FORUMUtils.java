package fr.inrae.toulouse.metexplore.kg4j.RDF.FORUMBinding;

import org.apache.jena.rdf.model.*;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.RDF.RDFUtils;
import fr.inrae.toulouse.metexplore.kg4j.data.*;

/**
 * Utility class to create default builders for various types of BioNode objects.
 * Provides static methods to create builders with default configurations for {@link BioConceptNode} and {@link BioMetaboliteNode} nodes.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class FORUMUtils {

    /**
     * Creates a default builder for constructing BioConceptNode objects with attributes retrieved from a given RDF model.
     * @param model the RDF model containing the data from which attributes are retrieved.
     * @return a BioNodeBuilder configured with default attributes for BioConceptNode nodes.
     */
    public static BioNodeBuilder<BioConceptNode> DefaultBioConceptNodeBuilder(Model model){
        BioNodeBuilder<BioConceptNode> builder = new BioNodeBuilder<>(model, BioConceptNode::new);
        builder.addField(BioConceptNode::setId, RDFNode::toString);
        builder.addField(BioConceptNode::setName, node -> RDFUtils.getAttribute(node.toString(), "http://www.w3.org/2000/01/rdf-schema#label", model, (x) -> x.toString().replace("@en", "")));
        builder.addField(BioConceptNode::addMeSHTreeNb, node -> RDFUtils.getAttribute(node.toString(), "http://id.nlm.nih.gov/mesh/vocab#treeNumber", model, RDFNode::toString));
        builder.addField(BioConceptNode::addMeSHParentTreeNb, node -> RDFUtils.getAttribute(node.toString(), "http://id.nlm.nih.gov/mesh/vocab#parentTreeNumber", model, RDFNode::toString));
        return builder;
    }

    /**
     * Creates a default builder for constructing BioConceptNode objects with attributes retrieved from a given RDF model.
     * @param model the RDF model containing the data from which attributes are retrieved.
     * @return a BioNodeBuilder configured with default attributes for BioMetaboliteNode nodes.
     */
    public static BioNodeBuilder<BioMetaboliteNode> DefaultBioMetaboliteNodeBuilder(Model model){
        BioNodeBuilder<BioMetaboliteNode> builder = new BioNodeBuilder<>(model, BioMetaboliteNode::new);
        builder.addField(BioMetaboliteNode::setId, node -> node.toString());
        builder.addField(BioMetaboliteNode::setName, node -> RDFUtils.getAttribute(node.toString(), "http://www.w3.org/2000/01/rdf-schema#label", model, (x) -> x.toString()));
        return builder;
    }

    /**
     * Creates a SimpleSelector for selecting MeSH concepts from the given RDF model.
     * @param model The RDF model.
     * @return A SimpleSelector for MeSH concepts.
     */
    public static SimpleSelector DefaultMeSHSelector(Model model){
        SimpleSelector meshS = new SimpleSelector(null,
                null,
                model.getProperty("http://id.nlm.nih.gov/mesh/vocab#TopicalDescriptor"));
        return meshS;
    }

    /**
     * Creates a SimpleSelector for selecting compounds from the given RDF model.
     * @param model The RDF model.
     * @return A SimpleSelector for compounds.
     */
    public static SimpleSelector DefaultCompoundSelector(Model model){
        SimpleSelector compoundS = new SimpleSelector(null,
                model.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                model.getProperty("http://www.w3.org/2002/07/owl#Class"));
        return compoundS;
    }

    /**
     * Creates a SimpleSelector for selecting SKOS related relations from the given RDF model.
     * @param model The RDF model.
     * @return A SimpleSelector for SKOS related relations.
     */
    public static SimpleSelector DefaultRelationSelector(Model model){
        SimpleSelector relationS = new SimpleSelector(null,
                model.getProperty("http://www.w3.org/2004/02/skos/core#related"),
                (RDFNode) null);
        return relationS;
    }

    /**
     * Creates a SimpleSelector for selecting RDF subClassOf relations from the given RDF model.
     * @param model The RDF model.
     * @return A SimpleSelector for RDF subClassOf relations.
     */
    public static SimpleSelector MeSHSubClassSelector(Model model){
        SimpleSelector subClassS = new SimpleSelector(null,
                model.getProperty("http://www.w3.org/2000/01/rdf-schema#subClassOf"),
                (RDFNode) null);
        return subClassS;
    }
}
