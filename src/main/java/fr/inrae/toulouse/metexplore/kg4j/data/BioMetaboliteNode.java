package fr.inrae.toulouse.metexplore.kg4j.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class defining a BioMetaboliteNode object, subclass of {@link BioNode}
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioMetaboliteNode extends BioNode {
    public String id;
    public String name;
    public Double degree;
    public List<String> nodeTypes = new ArrayList<>();

    /**
     * Class constructor specifying the new BioMetaboliteNode object's ID
     * @param id node ID
     */
    public BioMetaboliteNode(String id, String name) {
        super(id, name);
        this.id = id;
        this.name = name;
        nodeTypes.add("metabolite");
    }

    /**
     * Class constructor with a randomly generated ID
     * Uses {@link UUID#randomUUID()} to generate a unique identifier
     */
    public BioMetaboliteNode() {
        super(null, null);
        nodeTypes.add("metabolite");
    }

    /**
     * Get label
     * @return BioMetaboliteNode object's name (String)
     */
    public String getName(){
        return this.name;
    }

    /**
     * Get ID
     * @return BioMetaboliteNode object's ID (String)
     */
    public String getId(){
        return this.id;
    }

    /**
     * Get type
     * @return BioMetaboliteNode object's node type (String)
     */
    public List<String> getNodeTypes(){
        return this.nodeTypes;
    }

    /**
     * Get degree
     * @return The degree of this node.
     */
    public Double getDegree() {
        return degree;
    }

    /**
     * Set name
     * @param name The name to set.
     */
    @Override
    public void setName(String name){
        this.name = name;
    }

    /**
     * Set degree
     * @param degree The degree to set.
     */
    public void setDegree(Double degree){ this.degree = degree;}

    /**
     * Add type
     * @param type The type to add.
     */
    public void addType(String type) {
        this.nodeTypes.add(type);
    }

    /**
     * Set ID
     * @param id The ID to set.
     * @return This BioMetaboliteNode object.
     */
    public BioMetaboliteNode setId(String id){
        this.id = id;
        return this;
    }
}
