package fr.inrae.toulouse.metexplore.kg4j.RDF.FORUMBinding;

import org.apache.jena.rdf.model.Model;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.RDF.RDFUtils;
import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;

/**
 * Class implementing methods to build a BioConceptNode object.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioConceptNodeBuilder extends BioNodeBuilder<BioConceptNode> {

    /**
     * Class constructor.
     * Constructs a BioConceptNodeBuilder with the specified RDF model.
     * Initializes the BioConceptNode instances and defines the fields to extract attributes from the RDF model.
     * @param model the RDF model containing information about the nodes.
     */
    public BioConceptNodeBuilder(Model model) {
        super(model, BioConceptNode::new);
        // Define fields to extract attributes from the RDF model
        this.addField(BioConceptNode::setName, node -> RDFUtils.getAttribute(node.toString(), "http://www.w3.org/2000/01/rdf-schema#label", model, (x) -> x.toString()));
        this.addField(BioConceptNode::addMeSHTreeNb, node -> RDFUtils.getAttribute(node.toString(), "http://id.nlm.nih.gov/mesh/vocab#treeNumber", model, (x) -> x.toString()));
        this.addField(BioConceptNode::addMeSHParentTreeNb, node -> RDFUtils.getAttribute(node.toString(), "http://id.nlm.nih.gov/mesh/vocab#parentTreeNumber", model, (x) -> x.toString()));
        this.addField(BioConceptNode::setId, node -> node.toString());
    }
}
