package fr.inrae.toulouse.metexplore.kg4j.script;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Utility class for working with RDF models and SPARQL queries using Apache Jena.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class FORUMAPI {
    /**
     * Loads data into a Jena model using a SPARQL query.
     * @param queryFile Path to the file containing the SPARQL query.
     * @return The Jena model containing the query results.
     * @throws FileNotFoundException If the query file is not found.
     * @throws InterruptedException  If the thread is interrupted while sleeping.
     */
    public static Model loadModelSPARQLQuery(String queryFile) throws FileNotFoundException, InterruptedException {
        Scanner scanner = new Scanner(new File(queryFile));
        String query = scanner.useDelimiter("\\Z").next();
        Model model = ModelFactory.createDefaultModel();

        int batchSize = 1000000;
        int offset = 0;
        boolean hasMoreData = true;
        int batchNb = 0;

        while(hasMoreData) {
            batchNb += 1;
            Thread.sleep(1000);
            // Add limit and offset parameters to query string
            String queryString = query + "LIMIT " + batchSize + "OFFSET " + offset;
            // Execute query + load results in JENA model
            Query q = QueryFactory.create(queryString);
            try(QueryExecution qexec = QueryExecutionFactory
                    .sparqlService("https://forum.semantic-metabolomics.fr/sparql", q)) {
                Model resultModel = qexec.execConstruct();
                model.add(resultModel);
                offset += batchSize;
                hasMoreData = !model.isEmpty();
                System.out.println("batch " + batchNb);
            }
        }
        return model;
    }

    /**
     * Filters the model to include only statements with subjects matching the specified compounds.
     * @param model The original model.
     * @param fileName Path to the file containing compound IDs.
     * @return The filtered model.
     * @throws FileNotFoundException If the file containing compound IDs is not found.
     */
    public static Model filterCompounds(Model model, String fileName) throws FileNotFoundException {
        System.out.println("Filtering compounds ...");
        // Load compounds IDs from file
        Scanner scan = new Scanner(new File(fileName));
        ArrayList<String> compoundsList = new ArrayList<String>();
        while (scan.hasNext()){
            compoundsList.add(scan.next());
        }
        scan.close();
        // Filter model
        Model filteredModel = ModelFactory.createDefaultModel();
        StmtIterator iter = model.listStatements();
        while (iter.hasNext()) {
            Statement stmt = iter.nextStatement();
            Resource subject = stmt.getSubject();
            if(compoundsList.contains(subject.toString())){
                filteredModel.add(stmt);
            } else if (!subject.toString().contains("http://purl.obolibrary.org/obo/")) {
                filteredModel.add(stmt);
            }
        }
        System.out.println("Compounds filtered.");
        return filteredModel;
    }

    /**
     * Filters the model to include only statements related to specified MeSH concepts.
     * @param model The original model.
     * @param fileName Path to the file containing MeSH concepts.
     * @return The filtered model.
     * @throws FileNotFoundException If the file containing MeSH concepts is not found.
     */
    public static Model filterConcepts(Model model, String fileName) throws FileNotFoundException {
        System.out.println("Filtering concepts ...");
        Scanner scan = new Scanner(new File(fileName));
        ArrayList<String> meshList = new ArrayList<String>();
        while (scan.hasNext()){
            meshList.add(scan.next());
        }
        scan.close();

        ArrayList<String> compoundsList = new ArrayList<String>();
        Model filteredModel = ModelFactory.createDefaultModel().add(model);
        StmtIterator iter = model.listStatements();
        while (iter.hasNext()) {
            Statement stmt = iter.nextStatement();
            Resource subject = stmt.getSubject();
            RDFNode object = stmt.getObject();
            if(meshList.contains(subject.toString()) || meshList.contains(object.toString())){
                filteredModel.add(stmt);
            }
        }
        System.out.println("Concepts filtered.");
        return filteredModel;
    }

    /**
     * Adds concept-to-concept links to the model from a file.
     * @param model The original model.
     * @param fileName Path to the file containing concept-to-concept links.
     * @return The model with added links.
     * @throws FileNotFoundException If the file containing links is not found.
     */
    public static Model addConcept2ConceptLinks(Model model, String fileName) throws FileNotFoundException{
        System.out.println("Adding concept to concept links ...");
        // Load concept-to-concept links from file
        Model linkedModel = ModelFactory.createDefaultModel().add(model);
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split("\t");
                Statement newLink = ResourceFactory.createStatement(model.getResource(values[0]),
                        model.getProperty("http://www.w3.org/2004/02/skos/core#related"),
                        model.getResource(values[1]));
                linkedModel.add(newLink);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Links added.");
        return linkedModel;
    }

    /**
     * Adds MeSH thesaurus information to the model.
     * @param model The original model.
     * @return The model with added MeSH thesaurus information.
     */
    public static Model addMeSHThesaurus(Model model) {
        System.out.println("Adding MeSH thesaurus hierarchy ...");
        Model thesaurusModel = ModelFactory.createDefaultModel().add(model);
        // Load MeSH thesaurus info from file
        try (BufferedReader br = new BufferedReader(new FileReader("/home/mmathe/Documents/FORUM_SPARQL/MeSH_thesaurus_hierarchy.tsv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split("\t");
                Statement treeNumber = ResourceFactory.createStatement(model.getResource(values[0]),
                        model.createProperty("http://id.nlm.nih.gov/mesh/vocab#treeNumber"),
                        model.createLiteral(values[1]));
                Statement parentTreeNumber = ResourceFactory.createStatement(model.getResource(values[0]),
                        model.createProperty("http://id.nlm.nih.gov/mesh/vocab#parentTreeNumber"),
                        model.createLiteral(values[2]));
                thesaurusModel.add(treeNumber);
                thesaurusModel.add(parentTreeNumber);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("MeSH thesaurus added.");
        return thesaurusModel;
    }

    //        Multimap<String, String> map = ArrayListMultimap.create();
//        Model linkedFilteredModelCheBi = ModelFactory.createDefaultModel().add(linkedFilteredModel);
//        StmtIterator iter = linkedFilteredModel.listStatements();
//        while (iter.hasNext()) {
//            Statement stmt = iter.nextStatement();
//            Resource subject = stmt.getSubject();
//            if(subject.getLocalName().startsWith("CHEBI_")){
//                Query q = QueryFactory.create("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "PREFIX chebi: <http://purl.obolibrary.org/obo/CHEBI_> " +
//                        "CONSTRUCT{ " +
//                        "chebi:" + subject.getLocalName().substring(subject.getLocalName().lastIndexOf("_") + 1) +" rdfs:subClassOf ?broaderClass . " +
//                        "} " +
//                        "WHERE { " +
//                        "chebi:" + subject.getLocalName().substring(subject.getLocalName().lastIndexOf("_") + 1) +" rdfs:subClassOf ?broaderClass . " +
//                        "FILTER(strstarts(STR(?broaderClass), 'http://purl.obolibrary.org/obo/CHEBI')) " +
//                        "}");
//                try(QueryExecution qexec = QueryExecutionFactory.sparqlService("https://forum.semantic-metabolomics.fr/sparql", q)) {
//                    Model model = qexec.execConstruct();
//                    linkedFilteredModelCheBi.add(model);
//                }
//                Query q = QueryFactory.create("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "PREFIX chebi: <http://purl.obolibrary.org/obo/CHEBI_> " +
//                        "SELECT ?broaderClass  " +
//                        "WHERE { " +
//                        "chebi:" + subject.getLocalName().substring(subject.getLocalName().lastIndexOf("_") + 1) +" rdfs:subClassOf ?broaderClass . " +
//                        "FILTER(strstarts(STR(?broaderClass), 'http://purl.obolibrary.org/obo/CHEBI')) " +
//                        "}");
//                try(QueryExecution qexec = QueryExecutionFactory.sparqlService("https://forum.semantic-metabolomics.fr/sparql", q)){
//                    ResultSet rs = qexec.execSelect();
//                    while(rs.hasNext()){
//                        QuerySolution s = rs.nextSolution();
//                        if(!map.containsEntry(subject.getLocalName(), s.get("?broaderClass").toString())) {
//                            map.put(subject.getLocalName(), s.get("?broaderClass").toString());
//                        }
//                    }
//                }
//            }
//        }

}
