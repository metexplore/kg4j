package fr.inrae.toulouse.metexplore.kg4j.RDF;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import org.apache.jena.rdf.model.*;
import fr.inrae.toulouse.metexplore.kg4j.data.BioKnowledgeGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;

import java.util.Hashtable;

import static java.util.Objects.isNull;

/**
 * Class implementing methods to build a BioKnowledgeGraph graph from a JENA model.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphBuilder {
    // JENA model
    public Model model;
    // BioKnowledgeGraph to build
    public BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> KG;
    // Hashtable to link BioNode objects and RDF nodes from the JENA model
    public Hashtable<RDFNode, BioNode> RDFtoBioNode;

    /**
     * Class constructor specifying the JENA model to use.
     * @param m JENA model used to build the graph.
     */
    public BioKnowledgeGraphBuilder(Model m){
        this.model = m;
        this.KG = new BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>>();
        this.RDFtoBioNode = new Hashtable<RDFNode, BioNode>();
    }

    /**
     * Get KG.
     * @return a BioKnowledgeGraph
     */
    public BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> getKG(){
        return this.KG;
    }

    /**
     * Adds nodes ({@link BioNode} objects) to a {@link BioKnowledgeGraph}.
     * @param s the selector : retrieve triplets from the JENA model.
     * @param nodeBuilder specify the type of node.
     */
    public void addNodes(SimpleSelector s, BioNodeBuilder nodeBuilder){
        Model m = this.model;
        BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> kg = this.KG;
        StmtIterator selection = m.listStatements(s);
        // Iterate over the triplets matching the selector
        while (selection.hasNext()){
            Statement stmt = selection.nextStatement();
            Resource resource = stmt.getSubject();
            RDFNode node = stmt.getObject();
            // Create a new node
            BioNode newNode = nodeBuilder.build(resource);
            //System.out.println("addNodes() resource : " + resource);
            // Fill the hashtable
            if(!this.RDFtoBioNode.containsKey(resource)) {
                this.RDFtoBioNode.put(resource, newNode);
                this.KG.addVertex(newNode);
            }
        }
    }

    /**
     * Adds edges to a BioKnowledgeGraph.
     * @param s the selector : retrieves triplets from the JENA model.
     * @param newEdgeLabel specify the type of edge.
     */
    public void addEdges(SimpleSelector s, String newEdgeLabel) {
        Model m = this.model;
        BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> kg = this.KG;
        StmtIterator selection = m.listStatements(s);
        // Iterate over the triplets matching the selector
        while (selection.hasNext()) {
            Statement stmt = selection.nextStatement();
            Resource source = stmt.getSubject();
            RDFNode target = stmt.getObject();
            // Create new edge
            if(!isNull(this.RDFtoBioNode.get(source)) && !isNull(this.RDFtoBioNode.get(target))){
                if(kg.getAllEdges(this.RDFtoBioNode.get(target),this.RDFtoBioNode.get(source)).isEmpty()){
                    BioEdge<BioNode, BioNode> newEdge = new BioEdge<>(this.RDFtoBioNode.get(source), this.RDFtoBioNode.get(target),null);
                    newEdge.setLabel(newEdgeLabel);
                    kg.addEdge(newEdge);
                }
            }
        }
    }

    /**
     * Transform the graph as a list of triplets : subject -- predicate -> object.
     * @return graph as a String.
     */
    public String graphToString() {
        String graph = "";
        // Iterates over all the edges in the graph
        for (BioEdge e : this.KG.edgeSet()) {
            BioNode source = this.KG.getEdgeSource(e);
            BioNode target = this.KG.getEdgeTarget(e);
            graph += source.getName() + " -- " + e.getLabel() + " -> " + target.getName() + "\n";
            // add more attributes to display
        }
        return graph;
    }
}
