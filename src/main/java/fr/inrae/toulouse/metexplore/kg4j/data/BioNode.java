package fr.inrae.toulouse.metexplore.kg4j.data;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioEntity;

import java.util.List;

/**
 * Abstract class representing a node in the knowledge graph, subclass of {@link BioEntity} (met4j).
 * Each node has a unique identifier (id), a name and multiple types.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public abstract class BioNode extends BioEntity {
    public String id;
    public String name;
    public List<String> nodeTypes;

    /**
     * Class constructor specifying the new BioNode object's ID.
     * @param id node ID.
     */
    public BioNode(String id, String name) {
        super(id, name);
        this.id = id;
        this.name = name;
    }

    /**
     * Get label.
     * @return BioNode object's name (String).
     */
    public abstract String getName();

    /**
     * Get ID.
     * @return BioNode object's ID (String).
     */
    public String getId(){
        return this.id;
    };

    /**
     * Set ID.
     */
    public abstract BioNode setId(String id);

    /**
     * Get type.
     * @return BioNode object's node type (String).
     */
    public abstract List<String> getNodeTypes();

    /**
     * Add type.
     * @param type the type to add.
     */
    public abstract void addType(String type);
}
