package fr.inrae.toulouse.metexplore.kg4j;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import fr.inrae.toulouse.metexplore.kg4j.RDF.FORUMBinding.BioConceptNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the {@link BioConceptNodeBuilder} class
 * Contains test cases to verify the functionality of the BioConceptNodeBuilder class to create BioConceptNode objects from RDF data.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioConceptNodeBuilderTest {
    private BioConceptNodeBuilder bioConceptNodeBuilder;
    private Model model;

    /**
     * Sets up the test environment.
     * Initializes an RDF model and a BioConceptNodeBuilder instance with test data.
     */
    @Before
    public void setUp() {
        // Create an RDF model and add data for testing
        model = RDFDataMgr.loadModel("endometriosis-test.rdf");
        // Initialize the BioConceptNodeBuilder with the test RDF model
        bioConceptNodeBuilder = new BioConceptNodeBuilder(model);
    }

    /**
     * Tests the {@link BioConceptNodeBuilder#build(RDFNode)} method.
     * Verifies that a BioConceptNode object can be built from the RDF model.
     */
    @Test
    public void testBuildBioConceptNode() {
        // Build a BioConceptNode object from the RDF model
        StmtIterator sIter = model.listStatements();
        // Breast Diseases
        RDFNode testNode = model.getResource("http://id.nlm.nih.gov/mesh/D001941");
        BioConceptNode bioConcept = bioConceptNodeBuilder.build(testNode);
        // Check if the attributes of the generated BioConceptNode object matches the expected values
        assertEquals("Breast Diseases@en", bioConcept.getName());
        assertEquals("http://id.nlm.nih.gov/mesh/D001941", bioConcept.getId());
        assertTrue(bioConcept.getNodeTypes().contains("concept"));
    }
}
