package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode} class
 * Contains test cases to verify the functionality of the BioConceptNode class for representing and manipulating BioConceptNode nodes in a graph.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioConceptNodeTest {

    private BioConceptNode bioConcept;

    /**
     * Sets up the test environment by initializing a BioConceptNode object, executed before each test method.
     */
    @Before
    public void setUp() {
        bioConcept = new BioConceptNode();
    }

    /**
     * Test case for the {@link BioConceptNode#setName(String)} method.
     * Verifies that the setName method correctly sets the label of the BioConceptNode object.
     */
    @Test
    public void testSetName() {
        String name = "BioConcept Test";
        bioConcept.setName(name);
        assertEquals(name, bioConcept.getName());
    }

    /**
     * Test case for the {@link BioConceptNode#setId(String)} method.
     * Verifies that the setId method correctly sets the ID of the BioConceptNode object.
     */
    @Test
    public void testSetId() {
        String testId = "id-test";
        bioConcept.setId(testId);
        assertEquals(testId, bioConcept.getId());
    }

    /**
     * Test case for building a BioConceptNode object without specifying attributes.
     * Verifies that a BioConceptNode object built without attributes has the default node type, a null ID and a null name.
     */
    @Test
    public void testBuildWithoutLabel() {
        BioConceptNode bioConceptNode = new BioConceptNode();
        assertTrue(bioConceptNode.getNodeTypes().contains("concept"));
        assertNull(bioConceptNode.getName());
        assertNull(bioConceptNode.getId());
    }

    /**
     * Test case for the {@link BioConceptNode#addType(String)} method.
     * Verifies that the addType method correctly adds a new type in the nodeTypes list of the BioConceptNode object.
     */
    @Test
    public void testAddType() {
        bioConcept.addType("testType");
        assertTrue(bioConcept.getNodeTypes().contains("testType"));
    }

    /**
     * Test case for the {@link BioConceptNode#getId()} method.
     * Verifies that the getId method correctly returns the ID of the BioConceptNode object.
     */
    @Test
    public void testGetId() {
        bioConcept.setId("testId");
        assertEquals("testId", bioConcept.getId());
    }

    /**
     * Test case for the {@link BioConceptNode#getName()} method.
     * Verifies that the getName method correctly returns the name of the BioConceptNode object.
     */
    @Test
    public void testGetName() {
        bioConcept.setName("testName");
        assertEquals("testName", bioConcept.getName());
    }

    /**
     * Test case for the {@link BioConceptNode#getNodeTypes()} method.
     * Verifies that the getNodeTypes method correctly returns the nodeTypes list of the BioConceptNode object.
     */
    @Test
    public void testGetNodeTypes() {
        assertTrue(bioConcept.getNodeTypes().contains("concept"));
    }

    /**
     * Test case for the {@link BioConceptNode#setDegree(Double)} and the {@link BioConceptNode#getDegree()} methods.
     * Verifies that the setDegree method correctly sets the node degree and that the getDegree method correctly retrieves it.
     */
    @Test
    public void testSetDegree(){
        Double testDegree = 15.65;
        this.bioConcept.setDegree(testDegree);
        assertEquals(testDegree, this.bioConcept.getDegree());
    }

    /**
     * Test case for the {@link BioConceptNode#getMeSHCategory()} method.
     * Verifies that the getMeSHCategory method correctly returns the MeSH thesaurus category of the BioConceptNode object.
     */
    @Test
    public void testGetMeSHCategory() {
        bioConcept.addMeSHTreeNb("C12.050.351.500.163");
        assertEquals("C", bioConcept.getMeSHCategory());
    }

    /**
     * Test case for the {@link BioConceptNode#getMeSHtreeNb()} method.
     * Verifies that the getMeSHTreeNb method returns correctly the number in the MeSH thesaurus of the BioConceptNode object.
     */
    @Test
    public void testGetMeSHTreeNb() {
        bioConcept.addMeSHTreeNb("C12.500");
        List<String> treeNumbers = bioConcept.getMeSHtreeNb();
        assertEquals(1, treeNumbers.size());
        assertTrue(treeNumbers.contains("C12.500"));
    }

    /**
     * Test case for the {@link BioConceptNode#getParentMeSHTreeNb()} method.
     * Verifies that the getParentMeSHTreeNb correctly returns the parent number in the MeSH thesaurus of the BioConceptNode object.
     */
    @Test
    public void testGetParentMeSHTreeNb() {
        bioConcept.addMeSHParentTreeNb("C12");
        List<String> parentTreeNumbers = bioConcept.getParentMeSHTreeNb();
        assertEquals(1, parentTreeNumbers.size());
        assertTrue(parentTreeNumbers.contains("C12"));
    }
}
