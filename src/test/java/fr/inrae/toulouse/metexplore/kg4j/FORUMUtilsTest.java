package fr.inrae.toulouse.metexplore.kg4j;

import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.RDF.FORUMBinding.FORUMUtils;
import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the {@link FORUMUtils} class.
 * Contains test cases to verify the functionality of the builder utility methods to create builders with default configurations
 * for BioConceptNode and BioMetaboliteNode nodes.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class FORUMUtilsTest {
    private Model model;

    /**
     * Sets up the test environment by loading an RDF model with test data, executed before each test method
     */
    @Before
    public void setUp() {
        // Create an RDF model and add some data for testing
        model = RDFDataMgr.loadModel("endometriosis-test.rdf");
    }

    /**
     * Test case for the {@link FORUMUtils#DefaultBioConceptNodeBuilder(Model)} (Model)} method.
     * Verifies that the default BioNode builder creates BioConceptNode objects with correct attributes based on the provided RDF model.
     */
    @Test
    public void testDefaultBioConceptNodeBuilder() {
        BioNodeBuilder<BioConceptNode> bioConceptNodeBuilder = FORUMUtils.DefaultBioConceptNodeBuilder(model);
        // Build a BioConceptNode object from the RDF model
        RDFNode testNode = model.getResource("http://id.nlm.nih.gov/mesh/D002277");
        BioConceptNode bioConcept = bioConceptNodeBuilder.build(testNode);
        // Check if the attributes of the generated BioConceptNode object matches the expected values
        assertEquals("Carcinoma", bioConcept.getName());
        assertEquals("http://id.nlm.nih.gov/mesh/D002277", bioConcept.getId());
        assertTrue(bioConcept.getNodeTypes().contains("concept"));
    }

    /**
     * Test case for the {@link FORUMUtils#DefaultBioMetaboliteNodeBuilder(Model)} method.
     * Verifies that the default BioNode builder creates BioMetaboliteNode objects with correct attributes based on the provided RDF model.
     */
    @Test
    public void testDefaultBioMetaboliteNodeBuilder() {
        BioNodeBuilder<BioMetaboliteNode> bioMetaboliteNodeBuilder = FORUMUtils.DefaultBioMetaboliteNodeBuilder(model);
        // Build a BioMetaboliteNode object from the RDF model
        RDFNode testNode = model.getResource("http://purl.obolibrary.org/obo/CHEBI_131906");
        BioMetaboliteNode bioMetabolite = bioMetaboliteNodeBuilder.build(testNode);
        // Check if the attributes of the generated BioMetaboliteNode object matches the expected values
        assertEquals("4-pyranones", bioMetabolite.getName());
        assertEquals("http://purl.obolibrary.org/obo/CHEBI_131906", bioMetabolite.getId());
        assertTrue(bioMetabolite.getNodeTypes().contains("metabolite"));
    }

    /**
     * Test case for the {@link FORUMUtils#DefaultMeSHSelector(Model)} method.
     * Verifies that the default Selector successfully retrieves MeSH terms from the provided RDF model.
     */
    @Test
    public void testDefaultMeSHSelector() {
        SimpleSelector selector = FORUMUtils.DefaultMeSHSelector(model);
        StmtIterator iterator = model.listStatements(selector);
        assertTrue(iterator.hasNext());
        Statement stmt = iterator.nextStatement();
        assertEquals("http://id.nlm.nih.gov/mesh/D001941", stmt.getSubject().getURI());
        assertEquals("http://id.nlm.nih.gov/mesh/vocab#TopicalDescriptor", stmt.getObject().toString());
    }

    /**
     * Test case for the {@link FORUMUtils#DefaultCompoundSelector(Model)} method.
     * Verifies that the default Selector successfully retrieves compounds from the provided RDF model.
     */
    @Test
    public void testDefaultCompoundSelector() {
        SimpleSelector selector = FORUMUtils.DefaultCompoundSelector(model);
        StmtIterator iterator = model.listStatements(selector);
        assertTrue(iterator.hasNext());
        Statement stmt = iterator.nextStatement();
        assertEquals("http://purl.obolibrary.org/obo/CHEBI_15420", stmt.getSubject().getURI());
        assertEquals("http://www.w3.org/2002/07/owl#Class", stmt.getObject().toString());
    }

    /**
     * Test case for the {@link FORUMUtils#DefaultRelationSelector(Model)} method.
     * Verifies that the default Selector successfully retrieves "related" relations from the provided RDF model.
     */
    @Test
    public void testDefaultRelationSelector() {
        SimpleSelector selector = FORUMUtils.DefaultRelationSelector(model);
        StmtIterator iterator = model.listStatements(selector);
        assertTrue(iterator.hasNext());
        Statement stmt = iterator.nextStatement();
        assertEquals("http://id.nlm.nih.gov/mesh/D001941", stmt.getSubject().getURI());
        assertEquals("http://id.nlm.nih.gov/mesh/D004715", stmt.getObject().toString());
    }

    /**
     * Test case for the {@link FORUMUtils#MeSHSubClassSelector(Model)} method.
     * Verifies that the Selector successfully retrieves the MeSh subclasses from the provided RDF model.
     */
    @Test
    public void testMeSHSubClassSelector() {
        model.add(model.getResource("http://id.nlm.nih.gov/mesh/D004715"),
                model.createProperty("http://www.w3.org/2000/01/rdf-schema#subClassOf"),
                model.createResource("http://id.nlm.nih.gov/mesh/D005831"));
        SimpleSelector selector = FORUMUtils.MeSHSubClassSelector(model);
        StmtIterator iterator = model.listStatements(selector);
        assertTrue(iterator.hasNext());
        Statement stmt = iterator.nextStatement();
        assertEquals("http://id.nlm.nih.gov/mesh/D004715", stmt.getSubject().getURI());
        assertEquals("http://id.nlm.nih.gov/mesh/D005831", stmt.getObject().toString());
    }
}
