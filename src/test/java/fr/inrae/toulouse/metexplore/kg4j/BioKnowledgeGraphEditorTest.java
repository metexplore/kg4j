package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import fr.inrae.toulouse.metexplore.kg4j.edit.BioKnowledgeGraphEditor;
import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphJSONImporter;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static junit.framework.TestCase.*;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.edit.BioKnowledgeGraphEditor} class.
 * Contains test cases to verify the functionality of the BioknowledgeGraphEditor class for editing and manipulating a graph.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphEditorTest {
    public BioKnowledgeGraphEditor graphEditor = new BioKnowledgeGraphEditor();

    /**
     * Sets up the test environment by loading a graph from a JSON file and initializing a BioKnowledgeGraphEditor object, executed before each test method.
     * @throws FileNotFoundException if the test graph JSON file is not found.
     */
    @Before
    public void setUp() throws FileNotFoundException {
        BioKnowledgeGraphJSONImporter importer = BioKnowledgeGraphJSONImporter.getDefaultImporter();
        Path path = Paths.get("src/test/resources/filteredModel.json");
        String json = new Scanner(path.toFile()).useDelimiter("\\Z").next();
        importer.build(json);
        graphEditor = new BioKnowledgeGraphEditor(importer.getKG());
    }

    /**
     * Test case for loading a graph from JSON and converting it to a Graph object.
     * Verifies that the graph is correctly loaded and contains the expected number of vertices and edges.
     */
    @Test
    public void testJSONToGraph() {
        assertNotNull(graphEditor.KG);
        assertEquals(486, graphEditor.KG.vertexSet().size());
        assertEquals(5373, graphEditor.KG.edgeSet().size());
    }

    /**
     * Test case for the {@link BioKnowledgeGraphEditor#addNewVertex(BioNode)} method.
     * Verifies that the new vertex is successfully added to the graph
     */
    @Test
    public void testAddNewVertex() {
        BioNode newVertex = new BioConceptNode();
        graphEditor.addNewVertex(newVertex);
        assertTrue(graphEditor.KG.vertexSet().contains(newVertex));
    }

    /**
     * Test case for the {@link BioKnowledgeGraphEditor#addNewBioEdge(String, String, String)} method.
     * Verifies that the new labeled edge is successfully added to the graph, between the 2 correct vertices.
     */
    @Test
    public void testAddNewBioEdge() {
        BioNode vertex1 = new BioConceptNode("id1", "testConcept");
        BioNode vertex2 = new BioMetaboliteNode("id2", "testMetabolite");
        graphEditor.addNewVertex(vertex1);
        graphEditor.addNewVertex(vertex2);
        graphEditor.addNewBioEdge(vertex1.getId(), vertex2.getId(), "related");
        assertEquals(5374, graphEditor.KG.edgeSet().size());
    }

    /**
     * Test case for the {@link BioKnowledgeGraphEditor#removeVertexAndEdges(String)} method.
     * Verifies that the specified vertex and its edges are successfully removed from the graph.
     */
    @Test
    public void testRemoveVertexAndEdges() {
        graphEditor.removeVertexAndEdges("http://id.nlm.nih.gov/mesh/D014591");
        assertEquals(485, graphEditor.KG.vertexSet().size());
        assertEquals(5345, graphEditor.KG.edgeSet().size());
    }

    /**
     * Test case for the {@link BioKnowledgeGraphEditor#graphToString()} method.
     * Verifies that the graph is correctly converted to a string.
     */
    @Test
    public void testGraphToString() {
        String graphString = graphEditor.graphToString();
        assertNotNull(graphString);
        assertTrue(graphString.contains("chebi:177298 -- related -> Fallopian Tube Diseases"));
    }
}
