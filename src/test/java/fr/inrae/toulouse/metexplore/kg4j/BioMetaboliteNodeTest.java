package fr.inrae.toulouse.metexplore.kg4j;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.io.BioNode2Attribute;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * This class contains unit tests for the {@link BioMetaboliteNode} class.
 * Validates the functionality of various methods in the BioMetaboliteNode class.
 * The tests primarily focus on setting and getting attributes of BioMetaboliteNode instances.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioMetaboliteNodeTest {

    private BioMetaboliteNode testMetabolite;
    private Model testModel;

    /**
     * Sets up the test environment before each test method is executed: initializes a BioMetaboliteNode instance with a test ID
     * and loads a test RDF model containing BioMetaboliteNode data.
     */
    @Before
    public void setUp() {
        this.testMetabolite = new BioMetaboliteNode("testId", "testMetabolite");
        this.testModel = RDFDataMgr.loadModel("endometriosis-test.rdf");
    }

    /**
     * Test case for the {@link BioMetaboliteNode#setName(String)} and {@link BioMetaboliteNode#getName()} methods in the BioMetaboliteNode class.
     * Verifies that the name attribute of the BioMetaboliteNode is set and returned correctly.
     */
    @Test
    public void testSetName() {
        String name = "Cancer";
        this.testMetabolite.setName(name);
        assertEquals(name, this.testMetabolite.getName());
    }

    /**
     * Test case for the {@link BioMetaboliteNode#setId(String)} and {@link BioMetaboliteNode#getId()} methods.
     * Verifies that the id attribute of the BioMetaboliteNode is set and returned correctly.
     */
    @Test
    public void testSetId() {
        String testId = "id";
        this.testMetabolite.setId(testId);
        assertEquals(testId, this.testMetabolite.getId());
    }

    /**
     * Test case for the {@link BioMetaboliteNode#setDegree(Double)} and {@link BioMetaboliteNode#getDegree()} methods.
     * Verifies that the degree attribute of tje BioMetaboliteNode is set and returned correctly.
     */
    @Test
    public void testSetDegree(){
        Double testDegree = 15.65;
        this.testMetabolite.setDegree(testDegree);
        assertEquals(testDegree, this.testMetabolite.getDegree());
    }

    /**
     * Test case for the {@link BioMetaboliteNode#addType(String)} and {@link BioMetaboliteNode#getNodeTypes()} methods.
     * Verifies that multiple types can be added and returned correctly as a list.
     */
    @Test
    public void testAddType() {
        String newType = "toto";
        this.testMetabolite.addType(newType);
        assertTrue(this.testMetabolite.getNodeTypes().contains(newType));
    }

    /**
     * Test case for the {@link BioMetaboliteNode#getId()} method.
     * Verifies that the node's id is correctly returned.
     */
    @Test
    public void testGetId() {
        assertEquals(this.testMetabolite.id, this.testMetabolite.getId());
    }

    /**
     * Test case for the {@link BioNode2Attribute#getDefaultBioMetaboliteNode2Attributes()} method.
     * Sets various attributes for the test BioMetaboliteNode instance and verifies if they are converted correctly.
     */
    @Test
    public void testNode2Attributes() {
        this.testMetabolite.setName("4-pyranones");
        this.testMetabolite.setId("testId");
        this.testMetabolite.addType("toto");
        BioNode2Attribute<BioMetaboliteNode> converter = BioNode2Attribute.getDefaultBioMetaboliteNode2Attributes();
        assertTrue(this.testMetabolite.getNodeTypes().contains(converter.getListStringAttributes(testMetabolite).get("type")));
        assertEquals(converter.getStringAttributes(testMetabolite).get("id"), this.testMetabolite.getId());
        assertEquals(converter.getStringAttributes(testMetabolite).get("name"), this.testMetabolite.getName());
    }

    /**
     * Test case for the {@link BioNode2Attribute#getNeo4jBioMetaboliteNode2Attribute()} method.
     * Sets various attributes for the test BioMetaboliteNode instance and verifies if they are converted correctly.
     */
    @Test
    public void testNeo4jNode2Attributes(){
        this.testMetabolite.setName("glucose");
        this.testMetabolite.setId("testId");
        this.testMetabolite.setDegree(15.65);
        BioNode2Attribute<BioMetaboliteNode> converter = BioNode2Attribute.getNeo4jBioMetaboliteNode2Attribute();
        assertTrue(this.testMetabolite.getNodeTypes().contains(converter.getListStringAttributes(testMetabolite).get("type")));
        assertEquals(converter.getStringAttributes(testMetabolite).get("id"), this.testMetabolite.getId());
        assertEquals(converter.getStringAttributes(testMetabolite).get("name"), this.testMetabolite.getName());
        assertEquals(converter.getDoubleAttributes(testMetabolite).get("degree"), this.testMetabolite.getDegree());
    }
}