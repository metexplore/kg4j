package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphExporter;
import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphJSONImporter;
import fr.inrae.toulouse.metexplore.kg4j.io.BioNode2Attribute;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static org.junit.Assert.assertNotNull;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.edit.BioKnowledgeGraphEditor} class.
 * Contains test cases to verify the functionality of the GraphExporter class for exporting a graph to JSON format.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphExporterTest {
    public BioKnowledgeGraphExporter bioKGExporter = new BioKnowledgeGraphExporter();

    /**
     * Sets up the test environment by loading a graph from a JSON file and initializing a BioKnowledgeGraphExporter object, executed before each test method.
     * @throws FileNotFoundException if the test graph JSON file is not found.
     */
    @Before
    public void setUp() throws FileNotFoundException {
        // create graph from test file
        BioKnowledgeGraphJSONImporter JSONimporter = new BioKnowledgeGraphJSONImporter();
        JSONimporter = BioKnowledgeGraphJSONImporter.getDefaultImporter();
        // load graph from JSON file
        Path path = Paths.get("src/test/resources/filteredModel.json");
        String json = new Scanner(path.toFile()).useDelimiter("\\Z").next();
        JSONimporter.build(json);
        this.bioKGExporter.KG = JSONimporter.getKG();
    }

    /**
     * Test case for the {@link BioKnowledgeGraphExporter#graphToJSON(String)} method.
     * Verifies that the exported JSON matches the expected JSON representation of the graph.
     * @throws IOException   if an I/O error occurs while reading or writing the JSON file.
     * @throws JSONException if there is an error in JSON processing.
     */
    @Test
    public void testGraphToJSON() throws IOException, JSONException {
        this.bioKGExporter.attResolver.put("metabolite", BioNode2Attribute.getDefaultBioMetaboliteNode2Attributes());
        this.bioKGExporter.graphToJSON("testGraphExporter.json");
        String expectedJson = new String(Files.readAllBytes(Paths.get("src/test/resources/filteredModel.json")));
        String actualJson = new String(Files.readAllBytes(Paths.get("testGraphExporter.json")));
        JSONAssert.assertEquals(expectedJson, actualJson, JSONCompareMode.LENIENT);
    }

    /**
     * Test case for retrieving the default GraphExporter object.
     * Verifies that the default exporter contains the expected attribute resolvers.
     */
    @Test
    public void testGetDefaultExporter() {
        BioKnowledgeGraphExporter exporter = BioKnowledgeGraphExporter.getDefaultExporter();
        // Ensure that the default exporter has the expected attribute resolvers
        assertNotNull(exporter.attResolver.get("metabolite"));
        assertNotNull(exporter.attResolver.get("concept"));
    }
}
