package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import fr.inrae.toulouse.metexplore.met4j_graph.core.BioGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphNeo4jJSONImporter;
import org.json.JSONArray;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphNeo4jJSONImporter} class.
 * Contains test cases to verify the functionality of the BioKnowledgeGraphNeo4jJSONImporter class for importing a graph from JSON format (exported from Neo4j.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphNeo4jJSONImporterTest {
    /**
     * Test case for the {@link BioKnowledgeGraphNeo4jJSONImporter#build(JSONArray, JSONArray)} method.
     * Verifies that the imported graph contains the expected nodes and edges.
     * @throws FileNotFoundException if the test graph JSON file is not found.
     */
    @Test
    public void testBuild() throws FileNotFoundException {
        // Build graph from JSON data
        BioKnowledgeGraphNeo4jJSONImporter importer = BioKnowledgeGraphNeo4jJSONImporter.getDefaultImporter();
        Path path = Paths.get("src/test/resources/test_import_neo4j.json");
        String json = new Scanner(path.toFile()).useDelimiter("\\Z").next();
        importer.build(json);
        BioGraph<BioNode, BioEdge<BioNode, BioNode>> graph = importer.getKG();
        // Ensure that the graph is not null
        assertNotNull(graph);
        // Lists of nodes and edges
        List<BioNode> bioNodes = new ArrayList<>();
        List<BioEdge<BioNode, BioNode>> edges = new ArrayList<>();
        // Add nodes to node list
        graph.vertexSet().forEach(vertex -> {
            if (vertex instanceof BioNode) {
                bioNodes.add(vertex);
            }
        });
        // Add edges to ege list
        graph.edgeSet().forEach(edges::add);
        // Assert that there is the right nb  of nodes and edges in the graph
        assertEquals(3, bioNodes.size());
        assertEquals(1, edges.size());
        // Assert that the right nodes and edges were added
        assertEquals("Uterine Diseases", bioNodes.get(0).getName());
        assertEquals("Reproduction", bioNodes.get(1).getName());
        assertEquals(null, edges.get(0).getLabel());
    }

    /**
     * Test case for the {@link BioKnowledgeGraphNeo4jJSONImporter#getDefaultImporter()} method.
     * Verifies that the default importer is initialized correctly with the expected node resolvers.
     */
    @Test
    public void testGetDefaultImporter() {
        BioKnowledgeGraphNeo4jJSONImporter importer = BioKnowledgeGraphNeo4jJSONImporter.getDefaultImporter();
        assertNotNull(importer);
        assertNotNull(importer.nodeResolver);
        assertEquals(2, importer.nodeResolver.size());
    }

    /**
     * Test cases for the getters and setters of the {@link BioKnowledgeGraphNeo4jJSONImporter} class.
     * Verifies that the keys for importing the graph from the JSON file are correctly set.
     */
    @Test
    public void testGettersAndSetters() {
        BioKnowledgeGraphNeo4jJSONImporter importer = BioKnowledgeGraphNeo4jJSONImporter.getDefaultImporter();
        // Test conceptsKey
        importer.setConceptsKey("newConceptsKey");
        assertEquals("newConceptsKey", importer.getConceptsKey());
        // Test compoundsKey
        importer.setCompoundsKey("newCompoundsKey");
        assertEquals("newCompoundsKey", importer.getCompoundsKey());
        // Test edgesKey
        importer.setEdgesKey("newEdgesKey");
        assertEquals("newEdgesKey", importer.getEdgesKey());
        // Test idKey
        importer.setIdKey("newIdKey");
        assertEquals("newIdKey", importer.getIdKey());
        // Test typeKey
        importer.setTypeKey("newTypeKey");
        assertEquals("newTypeKey", importer.getTypeKey());
        // Test nameKey
        importer.setNameKey("newNameKey");
        assertEquals("newNameKey", importer.getNameKey());
        // Test meshTreeNbKey
        importer.setMeshTreeNbKey("newMeshTreeNbKey");
        assertEquals("newMeshTreeNbKey", importer.getMeshTreeNbKey());
        // Test parentMeshTreeNbKey
        importer.setParentMeshTreeNbKey("newParentMeshTreeNbKey");
        assertEquals("newParentMeshTreeNbKey", importer.getParentMeshTreeNbKey());
        // Test degreeKey
        importer.setDegreeKey("newDegreeKey");
        assertEquals("newDegreeKey", importer.getDegreeKey());
        // Test sourceKey
        importer.setSourceKey("newSourceKey");
        assertEquals("newSourceKey", importer.getSourceKey());
        // Test targetKey
        importer.setTargetKey("newTargetKey");
        assertEquals("newTargetKey", importer.getTargetKey());
    }
}
