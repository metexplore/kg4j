package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.io.BioNodeBuilderFromJSON;
import org.json.JSONObject;
import org.junit.Test;

import java.util.function.BiConsumer;
import java.util.function.Function;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.io.BioNodeBuilderFromJSON} class
 * Contains test cases to verify the functionality of the BioNodeBuilderFromJSON class
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioNodeBuilderFromJSONTest {
    /**
     * Test case for the {@link BioNodeBuilderFromJSON#getNeo4jBioMetaboliteNodeBuilder()} method.
     * Verifies that the builder correctly builds a BioMetaboliteNode from a JSON file exported from Neo4j.
     */
    @Test
    public void testGetNeo4jBioMetaboliteNodeBuilder() {
        BioNodeBuilderFromJSON<BioMetaboliteNode> builder = BioNodeBuilderFromJSON.getNeo4jBioMetaboliteNodeBuilder();
        JSONObject json = new JSONObject();
        json.put("id", "metabolite1");
        json.put("name", "Test Metabolite");
        json.put("degree", 3.0);
        BioMetaboliteNode node = builder.build(json);
        assertNotNull(node);
        assertEquals("metabolite1", node.getId());
        assertEquals("Test Metabolite", node.getName());
        assertEquals(3.0, node.getDegree(), 0.0);
    }

    /**
     * Test case for the {@link BioNodeBuilderFromJSON#getNeo4jBioConceptNodeBuilder()} method.
     * Verifies that the builder correctly builds a BioConceptNode from a JSON file exported from Neo4j.
     */
    @Test
    public void testGetNeo4jBioConceptNodeBuilder() {
        BioNodeBuilderFromJSON<BioConceptNode> builder = BioNodeBuilderFromJSON.getNeo4jBioConceptNodeBuilder();
        JSONObject json = new JSONObject();
        json.put("id", "concept1");
        json.put("name", "Test Concept");
        json.put("degree", 2.0);
        json.put("meSHTreeNb", "M1234");
        json.put("meSHParentTreeNb", "P1234");
        BioConceptNode node = builder.build(json);
        assertNotNull(node);
        assertEquals("concept1", node.getId());
        assertEquals("Test Concept", node.getName());
        assertEquals(2.0, node.getDegree(), 0.0);
        assertTrue(node.getMeSHtreeNb().contains("M1234"));
        assertTrue(node.getParentMeSHTreeNb().contains("P1234"));
    }

    /**
     * Test case for the {@link BioNodeBuilderFromJSON#addField(BiConsumer, Function)} method.
     * Verifies that a given field can successfully be added to the node builder.
     */
    @Test
    public void testAddField() {
        BioNodeBuilderFromJSON<BioConceptNode> builder = new BioNodeBuilderFromJSON<>(BioConceptNode::new);
        builder.addField(BioConceptNode::setId, n -> n.getString("id"));
        builder.addField(BioConceptNode::setName, n -> n.optString("name", "?"));
        builder.addField(BioConceptNode::setDegree, n -> n.optDouble("degree", 0.0));
        JSONObject json = new JSONObject();
        json.put("id", "concept1");
        json.put("name", "Test Concept");
        json.put("degree", 1.5);
        BioConceptNode node = builder.build(json);
        assertNotNull(node);
        assertEquals("concept1", node.getId());
        assertEquals("Test Concept", node.getName());
        assertEquals(1.5, node.getDegree(), 0.0);
    }
}
