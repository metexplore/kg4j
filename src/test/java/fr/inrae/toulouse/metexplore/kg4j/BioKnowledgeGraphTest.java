package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioKnowledgeGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.data.BioKnowledgeGraph} class
 * Contains test cases to verify the functionality of the BioKnowledgeGraph class for creating and manipulating BioKnowledgeGraph entities.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphTest {
    private BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> bioKnowledgeGraph;
    private BioConceptNode node1;
    private BioMetaboliteNode node2;
    private BioEdge<BioNode, BioNode> edge;

    @Before
    public void setUp(){
        bioKnowledgeGraph = new BioKnowledgeGraph<>();
        node1 = new BioConceptNode();
        node2 = new BioMetaboliteNode();
        edge = new BioEdge<>(node1, node2, null);
    }

    @Test
    public void testCreateEdgeFromModel(){
        BioEdge<BioNode, BioNode> createdEdge = bioKnowledgeGraph.createEdgeFromModel(node1, node2, edge);
        assertNotNull(createdEdge);
        assertEquals(node1, createdEdge.getV1());
        assertEquals(node2, createdEdge.getV2());
    }

    @Test
    public void testCopyEdge(){
        BioEdge<BioNode, BioNode> copiedEdge = bioKnowledgeGraph.copyEdge(edge);
        assertNotNull(copiedEdge);
        assertEquals(node1, copiedEdge.getV1());
        assertEquals(node2, copiedEdge.getV2());
    }

/*    @Test
    public void testCreateVertex(){
        BioNode createdVertex = bioKnowledgeGraph.createVertex("test");
        assertNotNull(createdVertex);
        assertEquals("test", createdVertex.getId());
    }*/
}