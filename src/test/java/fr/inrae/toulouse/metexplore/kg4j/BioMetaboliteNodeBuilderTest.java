package fr.inrae.toulouse.metexplore.kg4j;

import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;
import fr.inrae.toulouse.metexplore.kg4j.RDF.FORUMBinding.BioMetaboliteNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the {@link BioMetaboliteNodeBuilder} class.
 * Contains test cases to verify the functionality of BioMetaboliteNodeBuilder class.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioMetaboliteNodeBuilderTest {
    private final Model testModel = RDFDataMgr.loadModel("endometriosis-test.rdf");
    private final BioMetaboliteNodeBuilder testBioMetaboliteBuilder = new BioMetaboliteNodeBuilder(testModel);

    /**
     * Test the {@link BioMetaboliteNodeBuilder#build(RDFNode)} method.
     * Verifies that the BioMetaboliteNodeBuilder can correctly build a BioMetaboliteNode from the RDF model.
     */
    @Test
    public void testBuild() {
        // Define a test selector to retrieve a specific BioNode node from the RDF model
        SimpleSelector testSelector = new SimpleSelector(null,
                this.testModel.getProperty("http://www.w3.org/2000/01/rdf-schema#label"),
                "Carcinoma");
        // Iterate through statements matching the test selector from the RDF model
        StmtIterator selection = this.testModel.listStatements(testSelector);
        while (selection.hasNext()) {
            Statement stmt = selection.nextStatement();
            RDFNode node = stmt.getObject();
            // Build a new BioNode using the BioMetaboliteBuilder
            BioNode newNode = this.testBioMetaboliteBuilder.build(node);
            // Verify that the label of the generated BioNode matches the expected value
            assertEquals("Carcinoma", newNode.getName());
        }
    }
}
