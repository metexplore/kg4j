package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import fr.inrae.toulouse.metexplore.kg4j.io.BioNode2Attribute;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.junit.Test;

import java.util.Map;
import java.util.function.Function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.io.BioNode2Attribute} class
 * Contains test cases to verify the functionality of the BioNode2Attribute class
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioNode2AttributeTest {

    /**
     * Tests the {@link fr.inrae.toulouse.metexplore.kg4j.io.BioNode2Attribute#addStringAttribute(String, Function)} method.
     * Verifies that string attributes can be added to the converter and retrieved correctly.
     */
    @Test
    public void testAddStringAttribute() {
        // Create a BioNode2Attribute converter
        BioNode2Attribute<BioMetaboliteNode> converter = new BioNode2Attribute<>();
        // Add string attributes to the converter
        converter.addStringAttribute("name", BioMetaboliteNode::getName);
        converter.addStringAttribute("id", BioMetaboliteNode::getId);
        // Create a BioMetaboliteNode object
        BioMetaboliteNode metabolite = new BioMetaboliteNode("metaboliteId","4-pyranones");
        // Get string attributes from the converter
        Map<String, String> stringAttributes = converter.getStringAttributes(metabolite);
        // Verify the retrieved string attributes
        assertTrue(stringAttributes.containsKey("name"));
        assertEquals("4-pyranones", stringAttributes.get("name"));
        assertEquals("metaboliteId", stringAttributes.get("id"));
    }

    /**
     * Tests the {@link BioNode2Attribute#getAttributes(BioNode)} method.
     * Verifies that attributes can be retrieved from the converter correctly.
     */
    @Test
    public void testGetAttributes() {
        // Create a BioNode2Attribute converter
        BioNode2Attribute<BioMetaboliteNode> converter = new BioNode2Attribute<>();
        // Add attributes to the converter
        converter.addStringAttribute("name", BioMetaboliteNode::getName);
        converter.addStringAttribute("id", BioMetaboliteNode::getId);
        // Create a BioMetaboliteNode object
        BioMetaboliteNode bioMetabolite = new BioMetaboliteNode("metaboliteId", "4-pyranones");
        // Get attributes from the converter
        Map<String, Attribute> attributes = converter.getAttributes(bioMetabolite);
        // Verify the retrieved attributes
        assertEquals(2, attributes.size());
        assertTrue(attributes.containsKey("name"));
        assertTrue(attributes.containsKey("id"));
        assertEquals(DefaultAttribute.createAttribute("4-pyranones"), attributes.get("name"));
        assertEquals(DefaultAttribute.createAttribute("metaboliteId"), attributes.get("id"));
    }
}
