package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.riot.RDFDataMgr;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioKnowledgeGraphBuilder;
import fr.inrae.toulouse.metexplore.kg4j.RDF.BioNodeBuilder;
import fr.inrae.toulouse.metexplore.kg4j.data.BioConceptNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioMetaboliteNode;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.RDF.BioKnowledgeGraphBuilder} class
 * Contains test cases to verify the functionality of the BioKnowledgeGraphBuilder class for building a graph from an RDF model
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphBuilderTest {
    private Model testModel;
    private BioKnowledgeGraphBuilder bioKGBuilder;
    private String expectedOutput;

    /**
     * Sets up the test environment by initializing a JENA RDF model and a BioKnowledgeGraphBuilder object, executed before each test method.
     */
    @Before
    public void setUp() {
        // Initialize a test JENA model
        testModel = RDFDataMgr.loadModel("endometriosis-test.rdf");
        // Initialize GraphBuilder with the test model
        bioKGBuilder = new BioKnowledgeGraphBuilder(testModel);
        // Add BioMetaboliteNode nodes
        BioNodeBuilder bioMetaboliteBuilder = new BioNodeBuilder<BioMetaboliteNode>(testModel, BioMetaboliteNode::new);
        SimpleSelector compoundSelector = new SimpleSelector(null,
                testModel.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                testModel.getProperty("http://www.w3.org/2002/07/owl#Class"));
        bioKGBuilder.addNodes(compoundSelector, bioMetaboliteBuilder);
    }

    /**
     * Test case for the {@link BioKnowledgeGraphBuilder#addNodes(SimpleSelector, BioNodeBuilder)} method.
     * Verifies that nodes are correctly added to the graph.
     */
    @Test
    public void testAddNodes() {
        // Define test Node and test Selector
        BioNodeBuilder testBioNodeBuilder = new BioNodeBuilder<BioConceptNode>(testModel, BioConceptNode::new);
        SimpleSelector testSelector = new SimpleSelector(null,
                null,
                testModel.getProperty("http://id.nlm.nih.gov/mesh/vocab#TopicalDescriptor"));
        // Number of nodes to add (known)
        int addedNodeCount = 3;
        // Get initial node count
        int initialNodeCount = bioKGBuilder.getKG().vertexSet().size();
        // addNodes method
        bioKGBuilder.addNodes(testSelector, testBioNodeBuilder);
        // Get expected node count
        int expectedNodeCount = bioKGBuilder.getKG().vertexSet().size();
        // Assert expected behavior
        assertEquals(expectedNodeCount, initialNodeCount + addedNodeCount);
    }

    /**
     * Test case for the {@link BioKnowledgeGraphBuilder#addEdges(SimpleSelector, String)} method.
     * Verifies that edges are correctly added to the graph.
     */
    @Test
    public void testAddEdges() {
        // Add BioMetaboliteNode nodes
        BioNodeBuilder bioMetaboliteBuilder = new BioNodeBuilder<BioMetaboliteNode>(testModel, BioMetaboliteNode::new);
        SimpleSelector compoundSelector = new SimpleSelector(null,
                testModel.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
                testModel.getProperty("http://www.w3.org/2002/07/owl#Class"));
        bioKGBuilder.addNodes(compoundSelector, bioMetaboliteBuilder);
        // Define test Node and test Selector
        BioNodeBuilder testBioNodeBuilder = new BioNodeBuilder<BioConceptNode>(testModel, BioConceptNode::new);
        SimpleSelector testSelector = new SimpleSelector(null,
                null,
                testModel.getProperty("http://id.nlm.nih.gov/mesh/vocab#TopicalDescriptor"));
        bioKGBuilder.addNodes(testSelector, testBioNodeBuilder);
        // add Edges
        SimpleSelector relationSelector = new SimpleSelector(null,
                testModel.getProperty("http://www.w3.org/2004/02/skos/core#related"),
                (RDFNode) null);
        // Nb of edges to add (known)
        int addedEdgeCount = 3;
        // Get nb of edges in graph
        int initialEdgeCount = bioKGBuilder.getKG().edgeSet().size();
        // Define test edges and selector
        String testEdgeLabel = "related";
        bioKGBuilder.addEdges(relationSelector, testEdgeLabel);
        // Get expected edge count
        int expectedEdgeCount = bioKGBuilder.getKG().edgeSet().size();
        // Assert expected behaviour
        assertEquals(expectedEdgeCount, initialEdgeCount + addedEdgeCount);
    }

    /**
     * Test case for the {@link BioKnowledgeGraphBuilder#graphToString()} method.
     * Verifies that the graph is correctly converted to a string representation.
     */
    @Test
    public void testGraphToString() {
        expectedOutput = "";
        for (BioEdge e : bioKGBuilder.getKG().edgeSet()) {
            BioNode source = bioKGBuilder.getKG().getEdgeSource(e);
            BioNode target = bioKGBuilder.getKG().getEdgeTarget(e);
            expectedOutput += source.getName() + " -- " + e.getLabel() + " -> " + target.getName() + "\n";
            // add more attributes to display
        }
        assertEquals(expectedOutput, bioKGBuilder.graphToString());
    }
}
