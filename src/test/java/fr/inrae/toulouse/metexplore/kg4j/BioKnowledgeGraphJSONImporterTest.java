package fr.inrae.toulouse.metexplore.kg4j;

import fr.inrae.toulouse.metexplore.met4j_graph.core.BioEdge;
import fr.inrae.toulouse.metexplore.met4j_graph.core.BioGraph;
import fr.inrae.toulouse.metexplore.kg4j.data.BioNode;
import fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphJSONImporter;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit tests for the {@link fr.inrae.toulouse.metexplore.kg4j.io.BioKnowledgeGraphExporter} class.
 * Contains test cases to verify the functionality of the BioKnowledgeGraphJSONImporter class for importing a graph from JSON format.
 *
 * @author Meije Mathé
 * @author Clément Frainay
 */
public class BioKnowledgeGraphJSONImporterTest {
    /**
     * Test case for building a graph from JSON data.
     * Verifies that the imported graph contains the expected nodes and edges.
     * @throws FileNotFoundException if the test graph JSON file is not found.
     */
    @Test
    public void testBuild() throws FileNotFoundException {
        // Build graph from JSON data
        BioKnowledgeGraphJSONImporter importer = BioKnowledgeGraphJSONImporter.getDefaultImporter();
        Path path = Paths.get("src/test/resources/filteredModel.json");
        String json = new Scanner(path.toFile()).useDelimiter("\\Z").next();
        importer.build(json);
        BioGraph<BioNode, BioEdge<BioNode, BioNode>> graph = importer.getKG();
        // Ensure that the graph is not null
        assertNotNull(graph);
        // Lists of nodes and edges
        List<BioNode> bioNodes = new ArrayList<>();
        List<BioEdge<BioNode, BioNode>> edges = new ArrayList<>();
        // Add nodes to node list
        graph.vertexSet().forEach(vertex -> {
            if (vertex instanceof BioNode) {
                bioNodes.add(vertex);
            }
        });
        // Add edges to ege list
        graph.edgeSet().forEach(edges::add);
        // Assert that there is the right nb  of nodes and edges in the graph
        assertEquals(486, bioNodes.size());
        assertEquals(5373, edges.size());
        // Assert that the right nodes and edges were added
        assertEquals("Uterine Diseases", bioNodes.get(0).getName());
        assertEquals("Growth Inhibitors", bioNodes.get(1).getName());
        assertEquals("related", edges.get(0).getLabel());
    }

    /**
     * Test case for the {@link BioKnowledgeGraphJSONImporter#getDefaultImporter()} method.
     * Verifies that the default importer is initialized correctly with the expected node resolvers.
     */
    @Test
    public void testGetDefaultImporter() {
        BioKnowledgeGraphJSONImporter importer = BioKnowledgeGraphJSONImporter.getDefaultImporter();
        assertNotNull(importer);
        assertNotNull(importer.nodeResolver);
        assertEquals(2, importer.nodeResolver.size());
    }

    /**
     * Test cases for the getters and setters methods of the {@link BioKnowledgeGraphJSONImporter} class.
     * Verifies that the keys for importing the graph from the JSON file are correctly set.
     */
    @Test
    public void testGettersAndSetters() {
        BioKnowledgeGraphJSONImporter importer = BioKnowledgeGraphJSONImporter.getDefaultImporter();
        // Test multiTypeKey
        List<String> newMultiTypeKey = Arrays.asList("newType1", "newType2");
        importer.setMultiTypeKey(newMultiTypeKey);
        assertEquals(newMultiTypeKey, importer.getTypeKeys());
        // Test edgeLabelKey
        importer.setEdgeLabelKey("newEdgeLabelKey");
        assertEquals("newEdgeLabelKey", importer.getEdgeLabelKey());
        // Test sourceKey
        importer.setSourceKey("newSourceKey");
        assertEquals("newSourceKey", importer.getSourceKey());
        // Test targetKey
        importer.setTargetKey("newTargetKey");
        assertEquals("newTargetKey", importer.getTargetKey());
        // Test nodeKey
        importer.setNodeKey("newNodeKey");
        assertEquals("newNodeKey", importer.getNodeKey());
        // Test edgeKey
        importer.setEdgeKey("newEdgeKey");
        assertEquals("newEdgeKey", importer.getEdgeKey());
    }
}
