# Kg4j : The Java library for biomedical knowledge graphs  

Kg4j is an open-source Java library dedicated to the construction, edition and analysis of knowledge graphs focused on biomedical data.

## Usage

User documentation for the library can be found [here](https://forgemia.inra.fr/metexplore/kg4j/-/blob/main/User_documentation.md).  
Detailed code example can be found [here](https://forgemia.inra.fr/metexplore/kg4j/-/blob/main/src/main/java/fr/inrae/toulouse/metexplore/kg4j/script/Example.java).


## License
Kg4j is distributed under the open license [CeCILL-2.1](https://cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible GNU-GPL).
