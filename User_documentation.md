# kg4j

## User documentation

### Libraries

[met4j](https://forgemia.inra.fr/metexplore/met4j) : Library for the structural analysis of metabolic networks. It offers objects suited for the representation of biological entities.  
[JGraphT](https://jgrapht.org/) : Library that provides graph theory objects and algorithms.  
[Jena](https://jena.apache.org/) : Java framework for building semantic web applications. It groups together various APIs that interact with each other to process RDF data.  

### The BioNode class

The BioNode is an abstract class and is the parent class of all the classes describing biomedical entities in the knowledge graph inherit from the BioEntity class from met4j.
Each BioNode has these fields :
- id (String) : unique identifier. If it is null or empty during the construction of a BioNode, a unique random id is set.
- name (String).
- nodeTypes (List<String>) : a list of types that characterized the BioNode. A BioNode can be created with one or several types. The list of types can be edited after the BioNode creation.

### The BioMetaboliteNode class
The BioMetaboliteNode class inherits from the BioNode class and desribes a chemical compound. It has this additional field :  
- degree (Double) : degree centrality of the node in the graph (optional).    

Other fields can be added.  

**Create a BioMetaboliteNode**   

With a known ID and name:  
```{java}
BioMetaboliteNode compound = new BioMetaboliteNode("chebi:26764", "Steroid hormones");
```
Without:  
```{java}
BioMetaboliteNode compound = new BioMetaboliteNode();
```
When creating a BioMetaboliteNode without specifying an ID and a name, a randomly generated ID will be set and the name will be set as null.  
All BioMetaboliteNode automatically have the nodeTypes attribute set as: nodeTypes = ["metabolite"].  

**Add a BioMetaboliteNode to a BioKnowledgeGraph**  

Using the JGraphT method *addVertex*  
```{java}
kg.addVertex(compound);
```

**Remove a BioMetaboliteNode from a BioKnowledgeGraph**  

Using the JGraphT method *removeVertex*  
```{java}
kg.removeVertex(compound);
```

### The BioConceptNode class
The BioConceptNode class inherits from the BioNode class and describes a biomedical concept. It has these additional fields :  
- meshTreeNb (List<String>) : list of MeSH tree number, identifies the position of the concept in the MeSH thesaurus hierarchy.  
- meshParentTreeNb (List<String>) : list of parent MeSH tree number, identifies the parent concepts in the MeSH thesaurus hierarchy.  
- degree (Double) : degree centrality of the node in the graph (optional).  

Other fields can be added.  

**Create a BioConceptNode**

With a known ID and name :
```{java}
BioConceptNode concept = new BioConceptNode("D004715", "Endometriosis");
```  
Without :  
```{java}
BioConceptNode concept = new BioConceptNode();
```
When creating a BioConceptNode without specifying an ID and a name, a randomly generated ID will be set and the name will be set as null.  
All BioConceptNode automatically have the nodeTypes attribute set as: nodeTypes = ["concept"].  

**Add a BioConceptNode to a BioKnowledgeGraph**  

Using the JGraphT method *addVertex*  
```{java}
kg.addVertex(concept);
```

**Remove a BioConceptNode from a BioKnowledgeGraph**  

Using the JGraphT method *removeVertex*  
```{java}
kg.removeVertex(concept);
```

### The BioKnowledgeGraph class
It contains and links all the BioNode entities composing a knowledge graph (here : biomedical concepts, chemical compounds and the relations linking them all). These entities are linked together by BioEdge relations.

**Creation of a BioKnowledgeGraph from scratch**  

````{java}
BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> kg = new BioKnowledgeGraph<>();
````
Creates an empty BioKnowledgeGraph.  

### Construct a BioKnowledgeGraph
The first step is to construct a Jena model.   

**From a RDF file**  

Construct a Jena model from a RDF file.  
```{java}
Model model = RDFDataMgr.loadModel("test_graph.rdf");
```

**From a SPARQL query**  

Here, the Jena model is constructed by querying the RDF endpoint. It needs the SPARQL query to be written in a text file.  
For now, the default RDF endpoint is [FORUM](https://forum.semantic-metabolomics.fr/sparql) and the *loadModelSPARQLQuery* method from the FORUMAPI class is used.  
```{java}
Model model = loadModelSPARQLQuery("SPARQL_query.txt");
```

**Filter the Jena model**  

Methods are provided in the FORUMAPI class to apply filters on the BioKnowledgeGraph.  
- Filter the model to keep nodes from a list of IDs : here, only the nodes which IDs are in the text file are kept in the Jena model.    

```{java}
model = filterCompounds(model, "filter_compounds_ids.txt");
```

Then, the Jena model is converted to met4j entities.  

**Add nodes**  

Then, custom builders have to be defined to build the different BioNode entities that will be in the BioKnowledgeGraph.  
```{java}
BioNodeBuilder<BioConceptNode> conceptBuilder = FORUMUtils.DefaultBioConceptNodeBuilder(model);
BioNodeBuilder<BioMetaboliteNode> compoundBuilder = FORUMUtils.DefaultBioMetaboliteNodeBuilder(model);
```
Custom Selectors are also defined to target the information that will be used to construct the BioKnowledgeGraph from the Jena model.    
```{java}
SimpleSelector conceptSelector = FORUMUtils.DefaultMeSHSelector(model);
SimpleSelector compoundSelector = FORUMUtils.DefaultCompoundSelector(model);
```
It is also necessary to create a builder for the BioKnowledgeGraph with the Jena model as a parameter. This builder will use the model, the BioNode builders and the Selectors to build a BioKnowledgeGraph.   
```{java}
BioKnowledgeGraphBuilder KGBuilder = new BioKnowledgeGraphBuilder(model);
```
Finally, to construct the BioKnowledgeGraph, the different BioNode entities are added to the graph (here BioMetaboliteNode and BioConceptNode entities). To do so, the selector and the builder are used, for each type of entities, to construct BioNode object from the Jena Model and put them in the BioKnowledgeGraph entity.
```{java}
KGBuilder.addNodes(meshSelector, meshConceptBuilder);
KGBuilder.addNodes(compoundSelector, compoundBuilder);
```

**Add edges**  

As described above for the nodes, a custom selector for BioEdge objects has to be set up.
```{java}
SimpleSelector relationSelector = FORUMUtils.DefaultRelationSelector(model);
```
This selector is then used to add edges to the graph. A label for the relation can be specified.  
```{java}
KGBuilder.addEdges(relationSelector, "related");
```

To retrieve the new KG:  
```{java}
BioKnowledgeGraph<BioNode, BioEdge<BioNode, BioNode>> kg = KGBuilder.getKG();
```

### Edit a BioKnowledgeGraph
The class BioKnowledgeGraphEditor is a wrapper of JGraphT methods and provides new methods to edit a BioKnowledgeGraph entity.  
To edit a BioKnowledgeGraph, a BioKnowledgeGraphEditor needs to be instanciated with the KG that will be edited:  
```{java}
BioKnowledgeGraphEditor editor = new BioKnowledgeGraphEditor(kg);
```

**Remove a BioNode from a BioKnowledgeGraph**  

Remove a node and all the connecting vertex using the node's ID :  
```{java}
editor.removeVertexAndEdges(node2remove.getId());
```
If the node ID is unknown but another attribute that could be used tu find the node in the graph is known, it is possible to search for this node in the BioKnowledgeGraph and then remove it.  
For example, here we can search for a BioNode with a given name :  
```{java}
BioNode node2remove = KGBuilder.KG.vertexSet().stream().filter(bioNode -> bioNode.getName().equals("Glucose")).findAny().orElse(null);
```
If the node is not found in the gaph, it will be assigned a null value.  
Then, remove the node from the graph:  
```{java}
editor.removeVertexAndEdges(node2remove.getId());
```

**Add a BioNode to a BioKnowledgeGraph**  

Create a new node :  
```{java}
BioMetaboliteNode newMetabolite = new BioMetaboliteNode();
newMetabolite.setName("New compound");
newMetabolite.setId("compound");
```
Add the new node in the BioKnowledgeGraph :  
```{java}
KGeditor.addNewVertex(newMetabolite);
```

**Add an edge to a BioKnowledgeGraph**  

Set the source and target nodes. It is possible to either create new nodes or search for existing nodes in the graph :
```{java}
BioNode source = s;
BioNode target = t;
```
Add edge in the BioKnowledgeGraph. A label for the new relation has to be specified :  
```{java}
KGeditor.addNewBioEdge(source.getId(), target.getId(), "related");
```

### Export a BioKnowledgeGraph to a JSON file
To export a BioKnowledgeGraph to a JSON file a BioKnowledgeGraphJSONExporter needs to be instanciated.  
```{java}
BioKnowledgeGraphExporter KGExporter = new BioKnowledgeGraphExporter();
```
The KG that will be exported has to be defined in the exporter.  
```{java}
KGExporter.KG = kg;
```
Then, the "attribute resolver" needs to be defined. This is what will associate a node type with the attributes that should be exported in the JSON file. For example for a BioMetabolioteNode, the attributes will be ID, name, nodeTypes and degree.  
```{java}
KGExporter.attResolver.put("metabolite", BioNode2Attribute.getDefaultBioMetaboliteNode2Attributes());
KGExporter.attResolver.put("concept", BioNode2Attribute.getDefaultBioConceptNode2Attributes());
```
Default converters from nodes to attributes are already defined but they can be modified and others can be added.  
Finally the BioKnowledgeGraph is exported to a JSON file.   
```{java}
 KGExporter.graphToJSON("FILENAME.json");
 ```

### Import a BioKnowledgeGraph from a JSON file
To import a BioKnowledgeGraph from a JSON file a BioKnowledgeGraphJSONImporter needs to be instanciated.  
```{java}
BioKnowledgeGraphJSONImporter JSONimporter = BioKnowledgeGraphJSONImporter.getDefaultImporter();
```
A default importer is already defined, but others can be created. This default importer defines the different fields to read in the JSON file to create the different nodes in the graph.  
Read the JSON file :  
```{java}
Path path = Paths.get("file/path/FILENAME.json");
String json = new Scanner(path.toFile()).useDelimiter("\\Z").next();
```
Build the BioKnowledgeGraph from the JSON file :  
```{java}
JSONimporter.build(json);
```
